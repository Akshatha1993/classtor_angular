import {Component, Input, OnInit} from '@angular/core';
import {AClass, Answer} from '../../../../Shared/interfaces';
import {HttpService} from '../../../../core/http/http.service';
import {DataService} from '../../../../core/services/data.service';

@Component({
  selector: 'app-answer-feed',
  templateUrl: './answer-feed.component.html',
  styleUrls: ['./answer-feed.component.css']
})
export class AnswerFeedComponent implements OnInit {

  @Input() postId: string;
  answerFeedOfPost: Answer[];

  askQuestionMapArray;
  questions;
  answers = [];
  displayableQuestion;
  contraPartyObject = {
    contraPartyId: '',
    contraPartyName: ''
  };
  comments = [];
  commentArray = [];
  initialDataFromLimit = 0;
  initialDataToLimit = 9;

  constructor(private dataService: DataService,
              private httpService: HttpService) {
    this.dataService.getMapArrayToMessage.subscribe(response => {
      this.comments = [];
      this.answers = [];
      let convertedDateTime;
      let time;
      let convertedPoints;
      let othersAnswrs;
      this.askQuestionMapArray = response;
      if (this.askQuestionMapArray !== '') {
        if (this.askQuestionMapArray.get(localStorage.getItem('E_classId')).get(localStorage.getItem('mesUniqueId')) !== undefined) {
          othersAnswrs = this.askQuestionMapArray.get(localStorage.getItem('E_classId')).get(localStorage.getItem('mesUniqueId'));
          for (let i = 0; i < othersAnswrs.length; i++) {
            if (othersAnswrs[i].message.message.teacher_message === false) {
                this.answers.push(othersAnswrs[i]);
            }
          }
          if (this.answers.length > 0) {
            for (let j = 0; j < this.answers.length; j++) {
              time = this.answers[j].message.time.slice(0, -3);
              convertedDateTime = this.time2TimeAgo(j, Number(time));
              this.answers[j].message['timezone'] = convertedDateTime;
              if (this.answers[j].message.sender_points === null ||  this.answers[j].message.sender_points === 0) {
              } else {
                convertedPoints = this.logFunctionForExpertPoint(Number(this.answers[j].message.sender_points));
                this.answers[j].message['points'] = convertedPoints;
              }
            }
          }
        }

        // for (let k = 0; k < this.comments.length; k++) {
        //   time = this.comments[k].message.time.slice(0, -3);
        //   convertedDateTime = this.time2TimeAgo(k, Number(time));
        //   this.comments[k].message['timezone'] = convertedDateTime;
        // }
      }
    });

  }

  logFunctionForExpertPoint(points) {
    if (Math.round(Math.log10(points)) === 0) {
      return 0;
    } else {
      return Math.round(Math.log10(points));
    }
  }

  time2TimeAgo(i, time) {
    // This function computes the delta between the
    // provided timestamp and the current time, then test
    // the delta for predefined ranges.

    const d = new Date();  // Gets the current time
    const nowTs = Math.floor(d.getTime() / 1000); // getTime() returns milliseconds, and we need seconds, hence the Math.floor and division by 1000
    const seconds = nowTs - time;

    // morethan one year.
    if (seconds > 3.154e+7) {
      return Math.floor(seconds / 3.154e+7) + ' years ago';
    }

    // more that two days
    if (seconds > 2 * 24 * 3600) {
      return Math.floor(seconds / 86400) + ' days ago';
    }
    // a day
    // if (seconds > 24 * 3600) {
    //   console.log(Math.floor(seconds / 86400) + 'yesterday');
    //   return Math.floor(seconds / 86400) + ' yesterday';
    // }

    if (seconds > 3600) {
      return Math.floor(seconds / 3600) + ' hour ago';
    }
    // if (seconds > 1800) {
    //   console.log('Half an hour ago');
    // }
    if (seconds > 60) {
      return Math.floor(seconds / 60) + ' minute ago';
    }

    if (seconds < 60) {
      // return Math.floor(seconds) + ' seconds ago';
      return 'Just now';
    }
  }


  ngOnInit() {
    // this.answerFeedOfPost = this.getAnswerFeedByPostId();
  }


  // updateReadStatus() {
  //   if (localStorage.getItem('mesUniqueId') !== '') {
  //     let readStatusZeroArray;
  //     readStatusZeroArray = [];
  //     if (this.answers !== '') {
  //       for (let i = 0; i < this.answers.length; i++) {
  //         if (localStorage.getItem('E_UserID') !== this.answers[i].message.sender_id) {
  //           if (this.answers[i].read_status === 0) {
  //             readStatusZeroArray.push(this.answers[i].unique_id);
  //           }
  //         }
  //       }
  //     }
  //     if (readStatusZeroArray.length > 0) {
  //       this.dataService.setReadStatus(this.answers[0].message.parent);
  //       this.httpService.updateReadStatus(this.contraPartyId, readStatusZeroArray).subscribe(result => {
  //         console.log(result);
  //       });
  //     }
  //   }
  // }
}
