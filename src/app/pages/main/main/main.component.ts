import {Component, EventEmitter, Input, OnInit, Output, ViewChild, ElementRef, HostListener} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {DataService} from '../../../core/services/data.service';
import {HttpService} from '../../../core/http/http.service';
import {ActivatedRoute} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState,
} from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

declare var $: any;
@Component({
  selector: 'app-alt-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  // Sign In Step ?
  isSignedIn = false;
  isRegistration = false;
  @Input() classKey: string;
  @Output() EnterClass = new EventEmitter<string>();
  @ViewChild('drawer', {static: false}) drawer: any;
  private lastScrollTop: number = 0;
  private isAtTop: boolean = false;
  private element: any;

  @Input('refresh') inProgress: boolean = false;
  @Output() onPull: EventEmitter<any> = new EventEmitter<any>();
  public isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map((result: BreakpointState) => result.matches));

  lastViewedPostId: number;
  currentView;
  organizationName;
  userStatus = localStorage.getItem('E_UserStatus');
  data;
  showSpinner = false;

  // // For the Success Modal.
  // successModalMessage;
  // successModalHeading;
  // successModalBtnText;
  // successModalImgStatus;
  // successModalStatus = false;

  screensWhereYouCannotLeaveWithoutConfirmingChanges = [
    'POSTS',
    'MANAGE_CLASSROOM_STUDENTS',
    'MANAGE_CLASSROOM_INSTRUCTORS'
  ];

  constructor(private snackBar: MatSnackBar,
              private dataService: DataService,
              private httpService: HttpService,
              private route: ActivatedRoute,
              private spinner: NgxSpinnerService,
              private breakpointObserver: BreakpointObserver,
              el: ElementRef) {
    this.element = el.nativeElement;

    const queryString = window.location.search;
    if (queryString !== '') {
      const urlParams = queryString.split('&');
      const InstituteID = urlParams[0].split('=');
      if (InstituteID[0] === '?IID') {
        // localStorage.clear();
        this.currentView = 'LOGIN';
        // this.isSignedIn = false;

      } else {
        this.currentView = 'VERIFY-PASSCODE';
      }
    }

  }

  private get scrollTop() { return this.element.scrollTop || 0; }



  ngOnInit() {

    this.organizationName = localStorage.getItem('E_OrgName');
    // this.currentView = 'POSTS';

    if (localStorage.getItem('E_UserID') !== null) {
      this.currentView = 'POSTS';
      this.isSignedIn = true;
      localStorage.setItem('E_InitiateCommunication',  '0');
    }

    localStorage.setItem('searchkeyWord', 'null');

    // PullToRefresh.init({
    //   mainElement: document.getElementById('posts-view'),
    //   onRefresh: (done) => {
    //     setTimeout( () => {
    //       done(); // end pull to refresh
    //       this.dataService.setInitiateCommunication('0');
    //     }, 200);
    //   }
    // });
  }
  refreshScreen() {
    this.showSpinner = true;
    this.dataService.setInitiateCommunication('0');
    this.showSpinner = false;
  }
  closeSideNav() {
    if (this.drawer._mode === 'over') {
      this.drawer.close();
    }
  }
  onPostEnterListener(postId) {
    this.lastViewedPostId = postId;
    // Toggling currentView
    if (this.currentView !== 'SINGLE_POST') {
      this.snackBarCreator('Opening post #' + postId);
      this.currentView = 'SINGLE_POST';
    }
  }

  OnPostExitListener() {

    if (this.currentView === 'MANAGE_CLASSROOM_STUDENTS') {
      this.snackBarCreator('Finalize before switching screens.');
      this.currentView = 'POSTS';
    }
    // Toggling currentView
    if (this.currentView !== 'POSTS') {
      this.snackBarCreator('Returning to all posts');
      this.currentView = 'POSTS';
    }
  }

  onWantToQuestion() {
    // Toggling currentView
    if (this.currentView !== 'ASK_A_QUESTION') {
      this.currentView = 'ASK_A_QUESTION';
    }
  }

  onWantToFeed() {
    if (this.currentView !== 'POSTS') {
      this.currentView = 'POSTS';
    }
  }

  organizationregisterScreen() {
    if (this.currentView !== 'ORGNIZATION-REGISTER') {
      this.currentView = 'ORGNIZATION-REGISTER';
    }
  }

  OnGotoAllPosts() {
    if (this.currentView !== 'POSTS') {
      this.currentView = 'POSTS';
    }
  }
  // utility functions
  snackBarCreator(message: string) {
    this.snackBar.open(message, 'Close', {
      duration: 1000,
    });
  }

  onSuccessfullySignedIn() {
    this.isSignedIn = true;
    this.organizationName = localStorage.getItem('E_OrgName');
    this.userStatus = localStorage.getItem('E_UserStatus');
    this.currentView = 'POSTS';
  }

  registrationScreen() {
    this.isSignedIn = false;
    if (this.currentView !== 'REGISTRATION') {
      this.currentView = 'REGISTRATION';
    }
  }

  changePassScreen() {
    if (this.currentView !== 'CHANGEPASSWORD') {
      this.currentView = 'CHANGEPASSWORD';
    }
  }

  showDetail() {
    if (this.currentView !== 'SHOWDETAILS') {
      this.currentView = 'SHOWDETAILS';
    }
  }

  toLoginScreen() {
    if (this.currentView !== 'LOGIN') {
      this.currentView = 'LOGIN';
    }
  }

  logOut() {
    if (this.canChangeScreen() === false) { return; }
    localStorage.clear();
    this.isSignedIn = false;
    // if (this.currentView !== 'LOGIN') {
      this.currentView = 'LOGIN';
    // }
  }


  onSuccessfulRegistration() {
    if (this.currentView !== 'CONFIRMPASSCODE') {
      this.currentView = 'CONFIRMPASSCODE';
    }
  }
  forgotPassScreen() {
    if (this.currentView !== 'FORGOTPASS') {
      this.currentView = 'FORGOTPASS';
    }
  }

  createNewClass() {
    if (this.canChangeScreen() === false) { return; }
    this.currentView = 'CREATE_A_CLASS';
    this.snackBarCreator('Entering Create New Class');
  }

  enterClass(event) {
    if (this.canChangeScreen() === false) { return; }
    // this.snackBarCreator('Entering Class ' + classId);
    this.currentView = 'POSTS';
    this.EnterClass.emit(event);
  }


  gotoScreenAddStudentsToClassroom() {
    if (this.canChangeScreen() === false) { return; }
    this.currentView = 'MANAGE_CLASSROOM_STUDENTS';
  }

  gotoManageStudentsOfCurrentClassroom() {
    if (this.canChangeScreen() === false) { return; }
    this.currentView = 'MANAGE_CLASSROOM_STUDENTS';
  }

  gotoManageInstructorsOfCurrentChosenClassroom() {
    if (this.canChangeScreen() === false) { return; }
    this.currentView = 'MANAGE_CLASSROOM_INSTRUCTORS';
  }

  canChangeScreen() {
    // for (const screen of this.screensWhereYouCannotLeaveWithoutConfirmingChanges) {
    //       if (this.currentView === screen) {
    //         this.snackBarCreator('Finalize before switching screens.');
    //         return false;
    //       }
    //     }
    return true;
  }
}
