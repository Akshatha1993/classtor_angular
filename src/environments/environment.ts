// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const DOMAIN_NAME = 'http://backendless.io';
// export const DOMAIN_NAME = 'https://app.myclasstor.com/backendless_io';
export const PROJECT_PATH = DOMAIN_NAME + '/user/ceac10a0309e60457d2567b349fa872bc913dc2958b9b0a86052f8a2c0752e6e';
export const PROJECT_LOGIX_PATH =  PROJECT_PATH + '/logix/70edcc9efa97c30';
export const PROJECT_API_PATH =  PROJECT_PATH + '/api/70edcc9efa97c30';

export const PROJECT_GETINITIALDATAS_PATH = 'https://mytalent360.com:7055/getInitialDatas';
export const PROJECT_SENDMESSAGE_PATH = 'https://mytalent360.com:7050/sentMessage';
export const PROJECT_UPDATEREADSTATUS_PATH = 'https://mytalent360.com:7058/UpdateReadStatus';
export const PROJECT_CREATEGROUPMESSAGE_PATH = 'https://mytalent360.com:7057/createGroup';
export const PROJECT_GETGROUPDATAS_PATH = 'https://mytalent360.com:7055/getGroupDatas';
export const PROJECT_CONNECT_SOCKET_PATH = 'https://mytalent360.com:7056/ws';
export const PROJECT_SENDATTRIBUTE_PATH = 'https://mytalent360.com:7050/sentAttributes';
export const PROJECT_FETCHPOINTS_PATH = 'https://mytalent360.com:7057/fetchPoints';
export const PROJECT_DELETE_MESSAGE_PATH = 'https://mytalent360.com:7057/deleteMsg';
export const PROJECT_QUEIT = 'https://mytalent360.com:7080/queueIt';

// export const PROJECT_ID = '9f3134';
export const PROJECT_ID = 'b5076f';
export const environment = {
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
