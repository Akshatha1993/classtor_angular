import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataService} from '../../../../core/services/data.service';
import {HttpService} from '../../../../core/http/http.service';

export interface Post {
  postId: number;
  upvoted: boolean;
  upvoteCount: number;
  replyCount: number;
  lastActive: string;
  author: string;
  question: string;
  tag: string;
  profilePictureUrl: string;
  flagged: boolean;
}


@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit{

  @Input() classKey: string;
  @Output() enterIntoPostWithPostId = new EventEmitter();

  askQuestionMapArray;
  questions = [];
  questionsDetail = [];
  initialDataFromLimit = 0;
  initialDataToLimit = 19;
  showMyPostings = false;
  myPostingsData = [];
  searchResult = false;
  searchedQuestions = [];
  contraPartyId;

  constructor( private dataService: DataService,
               private httpService: HttpService) {
    // if (localStorage.getItem('E_InitiateCommunication') === '0') {
    //   this.dataService.setInitiateCommunication('0');
    // }

    this.dataService.getMapArrayToMessage.subscribe(response => {
      this.questions = [];
      this.askQuestionMapArray = response;
      this.showMyPostings = false;
      // this.searchResult = false;
      if (this.askQuestionMapArray !== '') {
        let dataServiceMapArray;
        if (localStorage.getItem('E_classId') !== null) {
          if (this.askQuestionMapArray.get(localStorage.getItem('E_classId')).get('0') !== undefined) {
            dataServiceMapArray = this.askQuestionMapArray.get(localStorage.getItem('E_classId')).get('0');
            for (let i = 0; i < dataServiceMapArray.length; i++) {
              if (dataServiceMapArray[i].message.category === localStorage.getItem('E_classId')) {
                this.questions.push(dataServiceMapArray[i]);
              }
            }
            for (let j = 0; j < this.questions.length; j++) {
              let convertedDateTime;
              let time;
              let convertedPoints;
              time = this.questions[j].message.time.slice(0, -3);
              convertedDateTime = this.time2TimeAgo(j, Number(time));
              this.questions[j].message['timezone'] = convertedDateTime;
            }
          }
          let xyz;
          xyz = this.questions;
          this.dataService.getSearchKeyWord.subscribe(response => {
            let apiResponse;
            if (response !== '') {
              apiResponse = response;
              if (apiResponse === true) {
                if (localStorage.getItem('searchkeyWord') !== null && localStorage.getItem('searchkeyWord') !== 'null') {
                  this.searchResult = true;
                  // this.questions = [];
                  this.searchedQuestions = [];
                  for (let i = 0; i < xyz.length; i++) {
                    if (xyz[i].message.message.msg.toLowerCase().indexOf(localStorage.getItem('searchkeyWord')) !== -1) {
                      this.searchedQuestions.push(xyz[i]);
                    }
                  }
                } else {
                  this.searchResult = false;
                }
              }
            }
          });
        }
      }
    });

    // let classid;
    // classid = JSON.parse(localStorage.getItem('E_ClassList'));
    // localStorage.setItem('E_classId', classid[1]);

    this.dataService.getCategoryMessages.subscribe(response => {
      let apiResponse;
      this.questions = [];
      let dataServiceMapArray;
      if (response !== '') {
        apiResponse = response;
        if (apiResponse === true) {
          if (localStorage.getItem('E_classId') !== null) {
            if (this.askQuestionMapArray.get(localStorage.getItem('E_classId')) !== undefined) {
              dataServiceMapArray = this.askQuestionMapArray.get(localStorage.getItem('E_classId')).get('0');
              for (let i = 0; i < dataServiceMapArray.length; i++) {
                if (dataServiceMapArray[i].message.category === localStorage.getItem('E_classId')) {
                  this.questions.push(dataServiceMapArray[i]);
                  if (localStorage.getItem('E_UserID') === dataServiceMapArray[i].message.sender_id) {
                    this.contraPartyId = dataServiceMapArray[i].message.receiver_id;
                  } else {
                    this.contraPartyId = dataServiceMapArray[i].message.sender_id;
                  }
                }
              }
              for (let j = 0; j < this.questions.length; j++) {
                let convertedDateTime;
                let time;
                let convertedPoints;
                time = this.questions[j].message.time.slice(0, -3);
                convertedDateTime = this.time2TimeAgo(j, Number(time));
                this.questions[j].message['timezone'] = convertedDateTime;
              }
            } else {
              this.questions = [];
            }
          }
        }
      }
    });

    // this.updateReadStatus();

    // this.dataService.getSearchKeyWord.subscribe(response => {
    //   let apiResponse;
    //   if (response !== '') {
    //     apiResponse = response;
    //     if (apiResponse === true) {
    //       this.searchResult = false;
    //       this.searchedQuestions = [];
    //     }
    //   //   else if (apiResponse === 'Unread') {
    //   //     this.searchedQuestions = [];
    //   //     this.searchResult = true;
    //   //     for (let i = 0; i < this.questions.length; i++) {
    //   //       if (this.questions[i].read_status === 0) {
    //   //         this.searchedQuestions.push(this.questions[i]);
    //   //       }
    //   //     }
    //   //   } else if (apiResponse === 'Read') {
    //   //     this.searchedQuestions = [];
    //   //     this.searchResult = true;
    //   //     for (let i = 0; i < this.questions.length; i++) {
    //   //       if (this.questions[i].read_status === 1) {
    //   //         this.searchedQuestions.push(this.questions[i]);
    //   //       }
    //   //     }
    //   //   } else if (apiResponse === 'All') {
    //   //     this.searchResult = false;
    //   //   }
    //   }
    // });

    this.dataService.getDeteletMessage.subscribe( response => {
      let apiResponse;
      if (response !== '') {
        apiResponse = response;
        if (this.searchResult === false) {
        for (let j = 0; j < this.questions.length; j++) {
          if (apiResponse.unique_id === this.questions[j].unique_id) {
            this.questions[j]['undo'] = true;
          }
        }
        } else {
          for (let j = 0; j < this.searchedQuestions.length; j++) {
            if (apiResponse.unique_id === this.searchedQuestions[j].unique_id) {
              this.searchedQuestions[j]['undo'] = true;
            }
          }
        }
      }
    });

    this.dataService.getUndoMessage.subscribe( response => {
      let apiResponse;
      if (response !== '') {
        apiResponse = response;
        for (let j = 0; j < this.questions.length; j++) {
          if (apiResponse.unique_id === this.questions[j].unique_id) {
            this.questions[j]['undo'] = false;
          }
        }
      }
    });

  }

  ngOnInit() {

  }

  onPostEnterListener(postId) {
    // Relaying
    this.enterIntoPostWithPostId.emit(
      postId
    );
  }

  time2TimeAgo(i, time) {
    // This function computes the delta between the
    // provided timestamp and the current time, then test
    // the delta for predefined ranges.

    const d = new Date();  // Gets the current time
    const nowTs = Math.floor(d.getTime() / 1000); // getTime() returns milliseconds, and we need seconds, hence the Math.floor and division by 1000
    const seconds = nowTs - time;

    // morethan one year.
    if (seconds > 3.154e+7) {
      return Math.floor(seconds / 3.154e+7) + ' years ago';
    }

    // more that two days
    if (seconds > 2 * 24 * 3600) {
      return Math.floor(seconds / 86400) + ' days ago';
    }
    // a day
    // if (seconds > 24 * 3600) {
    //   console.log(Math.floor(seconds / 86400) + 'yesterday');
    //   return Math.floor(seconds / 86400) + ' yesterday';
    // }

    if (seconds > 3600) {
      return Math.floor(seconds / 3600) + ' hour ago';
    }
    // if (seconds > 1800) {
    //   console.log('Half an hour ago');
    // }
    if (seconds > 60) {
      return Math.floor(seconds / 60) + ' minute ago';
    }

    if (seconds < 60) {
      return  'Just now';
    }
  }

  onWindowScroll() {
    if (window.pageYOffset > 200 && window.pageYOffset < 400) {
      this.loadMoreInitialData();
    }
  }

  updateReadStatus() {
      let readStatusZeroArray;
      readStatusZeroArray = [];
      if (this.questions !== []) {
        for (let i = 0; i < this.questions.length; i++) {
          if (localStorage.getItem('E_UserID') !== this.questions[i].message.sender_id) {
            if (this.questions[i].read_status === 0) {
              readStatusZeroArray.push(this.questions[i].unique_id);
            }
          }
        }
      }
      if (readStatusZeroArray.length > 0) {
        this.dataService.setReadStatus(this.questions[0].message.parent);
        this.httpService.updateReadStatus(this.contraPartyId, readStatusZeroArray).subscribe(result => {
        });
      }
    }

  loadMore() {
  this.loadMoreInitialData();
  }

  loadMoreInitialData() {
    this.initialDataFromLimit = this.initialDataToLimit + 1;
    this.initialDataToLimit =  this.initialDataFromLimit + 20;
    this.httpService.getGroupMessages(localStorage.getItem('E_UserID'), '0', this.initialDataFromLimit, this.initialDataToLimit, 'PARENTID',  '', JSON.parse(localStorage.getItem('E_socketIdConnection')).toString(),  '').subscribe(response => {
      let result;
      result = response;
    });
  }
}
