import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import * as sha1 from 'sha1/sha1';
import {DataService} from '../../../core/services/data.service';
import {HttpService} from '../../../core/http/http.service';
import { PROJECT_ID } from '../../../../environments/environment';

declare var $: any;
export interface Tag {
  id: string;
  tagName: string;
}

export interface SelectWithTag {
  id: string;
  viewValue: string;
  tags: Tag[];
}

export interface Select {
  id: string;
  userid: string;
  viewValue: string;
}


@Component({
  selector: 'app-post-question',
  templateUrl: './post-question.component.html',
  styleUrls: ['./post-question.component.css']
})
export class PostQuestionComponent implements OnInit{

  @Input() selectedTagId = -1;
  @Input() selectedGroupIdToPost = 1;
  @Input() selectedAnonymityScopeId = 1;
  @Output() toFeedList = new EventEmitter<string>();
  errorMessageStatus = false;
  public htmlContent;
  defaultGroup;
  showAddTag = true;
  fetchTagName;
  public groupId;
  userStatus;
  selectedClass;
  defaultTagName;

  editorConfig = {
    contenteditable: true,
    translate: 'no',
    toolbar: [
      ['bold', 'italic', 'underline', 'strikeThrough', 'superscript', 'subscript'],
      ['fontName', 'fontSize', 'color'],
      ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'indent', 'outdent'],
      ['cut', 'copy', 'delete', 'removeFormat', 'undo', 'redo'],
      ['paragraph', 'blockquote', 'removeBlockquote', 'horizontalLine', 'orderedList', 'unorderedList'],
    ]
  };

  defaultAnonymityScope = '1';
  anonymityScope: Select[] = [
    {id: '1', userid: localStorage.getItem('E_UserID'), viewValue: 'Anonymous'},
    {id: '2', userid: localStorage.getItem('E_UserID'), viewValue: localStorage.getItem('E_UserName')},
  ];

  classes = JSON.parse(localStorage.getItem('E_ClassList'));


  ngOnInit() {
    this.defaultGroup = localStorage.getItem('E_classId');
    this.userStatus = localStorage.getItem('E_UserClassType');
    this.getTag(this.defaultGroup);
  }

  constructor(private snackBar: MatSnackBar,
              private httpService: HttpService,
              private dataService: DataService) {}

  onSelectionChangedAnonymityLevel($event) {
    this.selectedAnonymityScopeId = $event.value;
    this.showSnackbarMessage('You will be posting as ' + this.getAnonymityScopeNameFromChosenAnonymityScopeId());
    
  }

  onSelectionChangedGroup($event) {
    this.selectedGroupIdToPost = $event.value;
    // Resetting Tag because tag corresponds to a particular class
    this.selectedTagId = -1;
    this.showSnackbarMessage('You will be posting to ' + this.getClassNameFromChosenClassId());
    
  }
 
  onTagChangedGroup($event) {
    this.selectedTagId = $event.value;
    this.showSnackbarMessage('Changed Tag to ' + this.getTagNameFromChosenTagId());
   
  }

  postQuestion() {
    if (this.htmlContent === undefined || this.defaultGroup === '' || this.selectedTagId === undefined) {
      this.errorMessageStatus = true;
    } else {
      let randomNum;
      let encryptedPassword;
      let hashedNum;
      let parentRandomNum;
      let parentEncryptedPassword;
      let parentHashedNum;
      let result;
      let receiverType;
      let messageType;
      let recieverName;
      let recieverId;
      let object;
      let senderId;
      let h;
      let unixTime;
      let timstamp;
      let stringTime;

      for (const aClass of this.classes) {
        if (aClass.group_name === this.getClassNameFromChosenClassId()) {
          recieverId = aClass.group_key;
        }
      }

      for (const anonymityScope of this.anonymityScope) {
        if (anonymityScope.viewValue === this.getAnonymityScopeNameFromChosenAnonymityScopeId()) {
          senderId = anonymityScope.userid;
        }
      }

      // hashedTopicId = sha1(PROJECT_ID + '#' + topicDetail.TopicID);
      // recieverid = (hashedTopicId.substring(0, 6));

      receiverType = 'I';
      messageType = 'TEXT';

      parentRandomNum = Math.floor(Math.random() * 90000) + 10000;
      parentEncryptedPassword = sha1(localStorage.getItem('E_UserID') + this.htmlContent + parentRandomNum);
      parentHashedNum = parentEncryptedPassword.substring(0, 15);

      let responseObject;
      responseObject = [{
        unique_id: parentHashedNum,
        message: {
          parent: '0',
          unique_id: parentHashedNum,
          project_id: PROJECT_ID,
          receiver_id: recieverId,
          receiver_name: this.getClassNameFromChosenClassId(),
          sender_name: this.getAnonymityScopeNameFromChosenAnonymityScopeId(),
          sender_id: senderId,
          sender_points: null,
          message_type: 'TEXT',
          message: {msg: this.htmlContent, tagName: this.getTagNameFromChosenTagId()},
          receiver_type: receiverType,
          time: '',
          category: recieverId,
          attribute: {
            like: {
              qty: '0',
              user: [],
            },
            dislike: {
              qty: '0',
              user: [],
            },
            comment: {
              qty: '0',
              user: [],
            },
            report: {
              qty: '0',
              user: [],
            }
          }

        },
        last_updated: ''
      }];
      unixTime = new Date();
      timstamp = unixTime.getTime();
      h = timstamp;
      stringTime = h.toString();

      responseObject[0].message.time = stringTime;
      responseObject[0].last_updated = stringTime;

      object = [
        {
          unique_id: parentHashedNum,
          receiver_type: receiverType,
          sender_name: this.getAnonymityScopeNameFromChosenAnonymityScopeId(),
          sender_id: senderId,
          receiver_name: this.getClassNameFromChosenClassId(),
          receiver_id: recieverId,
          message_type: messageType,
          message: {msg: this.htmlContent, tagName: this.getTagNameFromChosenTagId()},
          parent: '0',
          project_id: PROJECT_ID,
          random_key: parentRandomNum,
          category: recieverId,
          attribute: {
            like: {
              qty: '0',
              user: [],
            },
            dislike: {
              qty: '0',
              user: [],
            },
            comment: {
              qty: '0',
              user: [],
            },
            report: {
              qty: '0',
              user: [],
            }
          }
        }
      ];
      this.dataService.setMessageResObject(responseObject);
      this.htmlContent = '';
      if(localStorage.getItem('E_UserClassType') === '1') {
        this.tutorMessage(recieverId, this.getClassNameFromChosenClassId(), localStorage.getItem('E_UserID'), localStorage.getItem('E_UserName'));
      }
      this.toFeedList.emit();
      this.httpService.sendMessage(object).subscribe(response => {
        result = response;
        if (result.status === 'success') {
          this.htmlContent = '';
        }
      });
    }
  }

  tutorMessage(classid, classname, teacherid, teachername) {
    this.httpService.tutorMessage(classid, classname, teacherid, teachername, localStorage.getItem('E_OrgId'), localStorage.getItem('E_OrgName')).subscribe(response => {
    });
  }

  saveDraft() {
    this.showSnackbarMessage(
      'PSEUDO SAVE DRAFT' + '\n' +
      'HTML Content: ' + this.htmlContent + '\n' +
      'Class: ' + this.getClassNameFromChosenClassId() + '\n' +
      'Anonymity Level: ' + this.getAnonymityScopeNameFromChosenAnonymityScopeId() + '\n' +
      'Tag: ' + this.getTagNameFromChosenTagId()
    );
  }

  // Utility functions
  getClassNameFromChosenClassId() {
    for (const aClass of this.classes) {
      if (this.defaultGroup === aClass.group_key) {
        return aClass.group_name;
      } else {
        if (aClass.group_key === this.selectedGroupIdToPost.toString()) {
          return aClass.group_name;
        }
      }
    }
  }

  getTagNameFromChosenTagId() {
    for (const tag of this.fetchTagName) {
      if (this.defaultTagName === tag.tag_id) {
        return tag.tag_name;
      } else {
        if (tag.tag_id === this.defaultTagName.toString()) {
              return tag.tag_name;
            }
      }
      // for (const tag of aClass.tags) {
      //   if (tag.id === this.selectedTagId.toString()) {
      //     return tag.tagName;
      //   }
      // }
    }
  }

  getAnonymityScopeNameFromChosenAnonymityScopeId() {
    for (const anonymityScope of this.anonymityScope) {
      if (anonymityScope.id === this.selectedAnonymityScopeId.toString()) {
        return anonymityScope.viewValue;
      }
    }
  }

  getTagsOfAChosenClass() {
    for (const aClass of this.classes) {
      if (aClass.id === this.selectedGroupIdToPost.toString()) {
        return aClass.tags;
      }
    }
  }

  showSnackbarMessage(message: string) {
    this.snackBar.open(message, 'Close', {
      duration: 2000,
    });
  }
  // add Tag

  displayTag() {
    this.showAddTag = false;
  }

  getTag(event) {
    this.selectedClass = event;
    this.httpService.fetchTag(event).subscribe(
      response => {
        this.fetchTagName = response;
        this.fetchTagName = this.fetchTagName.response.result;
        this.defaultTagName = this.fetchTagName[0].tag_id;
        this.selectedClass = this.defaultGroup;
      });
  }
  
  //submit tag name

  submitTag(tagName) {
    let data = this.httpService.addTag(tagName, this.defaultGroup);
    data.subscribe(response => {
      let apiResponse;
      apiResponse = response;
      this.groupId = apiResponse.response['lastInsertedId'];
      if (apiResponse.response['status'] === 'success') {
        $.toast({
          heading: 'Success',
          text: 'Tag has been added successfully',
          bgColor: '#b3cde0',
          loaderBg: '#005b96',              // Background color for toast
          textColor: '#000',
          // text color
          allowToastClose: true,       // Show the close button or not
          hideAfter: 4000,              // `false` to make it sticky or time in miliseconds to hide after
          position: 'top-right',
          icon: 'success'
        });
        this.getUpdatedTags();
        this.defaultTagName = this.groupId.toString();
      }
    });
  }

  getUpdatedTags() {
    this.httpService.fetchTag(this.selectedClass).subscribe(
      response => {
        this.fetchTagName = response;
        this.fetchTagName = this.fetchTagName.response.result;

      });
  }

}
