import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../core/services/data.service';
import {HttpService} from '../../../core/http/http.service';
import {ActivatedRoute} from '@angular/router';

declare var $: any;
@Component({
  selector: 'app-verify-passcode',
  templateUrl: './verify-passcode.component.html',
  styleUrls: ['./verify-passcode.component.css']
})
export class VerifyPasscodeComponent implements OnInit {

  loginForm: FormGroup;
  email;
  passcode;
  loginErrorStatus = false;
  @Output() toSignInScreen = new EventEmitter();

  // For the Error Modal.
  errorModalMessage;
  errorModalHeading;
  errorModalBtnText;
  errorModalImgStatus;
  errorModalStatus = false;
  data;
  showLogo = false;

  constructor(private dataService: DataService,
              private httpService: HttpService,
              private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]),
      password: new FormControl('', [Validators.required, Validators.pattern(/^.{6,15}$/)]),
      passcode: new FormControl('', [Validators.required, Validators.pattern(/^.{4,4}$/)]),
    });

    this.dataService.getUserEmailAddress.subscribe(emailAddress =>
      this.email = emailAddress
    );

    const queryString = window.location.search.substring(1);
    if (queryString !== '') {
      let IID;
      const urlParams = queryString.split('&');
      const ID = urlParams[0].split('=');
      const PC = urlParams[1].split('=');
      if (urlParams[2] !== undefined) {
        IID = urlParams[2].split('=');
        if (IID[1] === 'e9Z5w8') {
          this.showLogo = true;
        }
      }
      this.email = ID[1];
      this.passcode = PC[1];
    }
  }


  onVerifyPasscode() {
    if (this.loginForm.value.email === '' || this.loginForm.value.password === '' || this.loginForm.value.passcode === '') {
      this.loginErrorStatus = true;
    } else {
      if (this.loginForm.valid === true) {
        this.httpService.userResetPassword(this.loginForm.value.email, this.loginForm.value.passcode,
          this.loginForm.value.password).subscribe(response => {
          let apiResponse = response;
          apiResponse = apiResponse['response']['result'];
          if (apiResponse['status'] !== 0) {

            if (apiResponse['status'] !== 0) {
              $.toast({
                heading: 'Success',
                text: 'Verified Passcode Successfully',
                bgColor: '#b3cde0',
                loaderBg: '#005b96',             // Background color for toast
                textColor: '#000',            // text color
                allowToastClose: true,       // Show the close button or not
                hideAfter: 4000,              // `false` to make it sticky or time in miliseconds to hide after
                position: 'top-right',
                icon: 'success'
              });
              this.toSignInScreen.emit();
            }
          } else {
            const errorCode = apiResponse['code']['Code'];
            const errorDesc = [];
            const errorMessage = apiResponse['description'];
            for (const value in errorMessage) {
              errorDesc.push(errorMessage[value]);
            }
            this.errorModalHeading = 'Error';
            this.errorModalMessage = errorDesc[0];
            this.errorModalBtnText = '';
            this.errorModalImgStatus = true;
            this.errorModalStatus = true;
          }

        });
      }
    }
  }

  closeErrorModal() {
    this.errorModalStatus = false;
  }

}
