import {Component, Input, OnInit} from '@angular/core';
import {Answer} from "../../../../../Shared/interfaces";
import {DataService} from "../../../../../core/services/data.service";
import {HttpService} from "../../../../../core/http/http.service";

@Component({
  selector: 'app-teacher-feed',
  templateUrl: './teacher-feed.component.html',
  styleUrls: ['./teacher-feed.component.css']
})
export class TeacherFeedComponent implements OnInit {

  @Input() postId: string;
  answerFeedOfPost: Answer[];

  askQuestionMapArray;
  questions;
  answers = [];
  displayableQuestion;
  contraPartyObject = {
    contraPartyId: '',
    contraPartyName: ''
  };
  comments = [];
  commentArray = [];
  initialDataFromLimit = 0;
  initialDataToLimit = 9;

  constructor(private dataService: DataService,
              private httpService: HttpService) {
    this.dataService.getMapArrayToMessage.subscribe(response => {
      this.comments = [];
      this.answers = [];
      let convertedDateTime;
      let time;
      let convertedPoints;
      let teacherAnswers;
      this.askQuestionMapArray = response;
      if (this.askQuestionMapArray !== '') {
        if (this.askQuestionMapArray.get(localStorage.getItem('E_classId')).get(localStorage.getItem('mesUniqueId')) !== undefined) {
          teacherAnswers = this.askQuestionMapArray.get(localStorage.getItem('E_classId')).get(localStorage.getItem('mesUniqueId'));
          for (let i = 0; i < teacherAnswers.length; i++) {
            if (teacherAnswers[i].message.message.teacher_message === true) {
              this.answers.push(teacherAnswers[i]);
            }
          }
          if (this.answers.length > 0) {
            for (let j = 0; j < this.answers.length; j++) {
              time = this.answers[j].message.time.slice(0, -3);
              convertedDateTime = this.time2TimeAgo(j, Number(time));
              this.answers[j].message['timezone'] = convertedDateTime;
              if (this.answers[j].message.sender_points === null ||  this.answers[j].message.sender_points === 0) {
              } else {
                convertedPoints = this.logFunctionForExpertPoint(Number(this.answers[j].message.sender_points));
                this.answers[j].message['points'] = convertedPoints;
              }
            }
          }
        }

      }
    });

  }

  ngOnInit() {
  }

  logFunctionForExpertPoint(points) {
    if (Math.round(Math.log10(points)) === 0) {
      return 0;
    } else {
      return Math.round(Math.log10(points));
    }
  }

  time2TimeAgo(i, time) {
    // This function computes the delta between the
    // provided timestamp and the current time, then test
    // the delta for predefined ranges.

    const d = new Date();  // Gets the current time
    const nowTs = Math.floor(d.getTime() / 1000); // getTime() returns milliseconds, and we need seconds, hence the Math.floor and division by 1000
    const seconds = nowTs - time;

    // morethan one year.
    if (seconds > 3.154e+7) {
      return Math.floor(seconds / 3.154e+7) + ' years ago';
    }

    // more that two days
    if (seconds > 2 * 24 * 3600) {
      return Math.floor(seconds / 86400) + ' days ago';
    }
    // a day
    // if (seconds > 24 * 3600) {
    //   console.log(Math.floor(seconds / 86400) + 'yesterday');
    //   return Math.floor(seconds / 86400) + ' yesterday';
    // }

    if (seconds > 3600) {
      return Math.floor(seconds / 3600) + ' hour ago';
    }
    // if (seconds > 1800) {
    //   console.log('Half an hour ago');
    // }
    if (seconds > 60) {
      return Math.floor(seconds / 60) + ' minute ago';
    }

    if (seconds < 60) {
      // return Math.floor(seconds) + ' seconds ago';
      return 'Just now';
    }

  }

}
