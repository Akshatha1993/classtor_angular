import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {ObjectMapper} from 'json-object-mapper';
import {catchError} from 'rxjs/operators';
import {HttpService} from '../../../core/http/http.service';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  @Output() newPassword=new EventEmitter();
  @Output() loginScreen = new EventEmitter();
   constructor(private httpService:HttpService) { }
    public data;
    toLoginScreen(changePasswordDetails) {
      
     if(changePasswordDetails.newPasswordField===changePasswordDetails.RetypePasswordField){
      this.httpService.userChangePassword(changePasswordDetails.currentPasswordField,changePasswordDetails.newPasswordField,changePasswordDetails.RetypePasswordField).subscribe(response =>{
        this.data=response;
        this.loginScreen.emit();
   })    
     }else{
       alert("Password Missmacth");
     }
   
  }

  

  ngOnInit() {
  }

}
