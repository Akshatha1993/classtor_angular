import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../core/services/data.service';
import {HttpService} from '../../../core/http/http.service';

declare var $: any;
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  loginForm: FormGroup;
  email;
  loginErrorStatus = false;
  showLogo = false;
  @Output() successfullySignedIn = new EventEmitter();
  @Output() registrationScreen = new EventEmitter();
  @Output() forgotPassScreen = new EventEmitter();
  @Output() registerOrg = new EventEmitter();

  // For the Error Modal.
  errorModalMessage;
  errorModalHeading;
  errorModalBtnText;
  errorModalImgStatus;
  errorModalStatus = false;

  constructor(private dataService: DataService,
              private httpService: HttpService) {

  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]),
      password: new FormControl('', [Validators.required, Validators.pattern(/^.{6,15}$/)]),
    });

    this.dataService.getUserEmailAddress.subscribe(emailAddress =>
      this.email = emailAddress
    );

    const queryString = window.location.search.substring(1);
    if (queryString !== '') {
      const urlParams = queryString.split('&');
      const InstituteID = urlParams[0].split('=');
      let IID;
      if (urlParams[2] !== undefined) {
        IID = urlParams[2].split('=');
      }
      if (InstituteID[1] === 'e9Z5w8') {
        this.showLogo = true;
      } else if (IID[1] === 'e9Z5w8') {
        this.showLogo = true;
      }
    }
  }


  toRegistrationScreen() {
    this.registrationScreen.emit();
  }

  toForgotPasswordScreen() {
    this.forgotPassScreen.emit();
  }

  onSignIn() {
    if (this.loginForm.value.email === '' || this.loginForm.value.password === '') {
      this.loginErrorStatus = true;
    } else {
      if (this.loginForm.valid === true) {
        this.httpService.userLogin(this.loginForm.value.email,
          this.loginForm.value.password).subscribe(response => {
          let apiResponse = response;
          apiResponse = apiResponse['response']['result'];
          if (apiResponse['status'] !== 0) {

            if (apiResponse['status'] !== 0) {
              $.toast({
                heading: 'Success',
                text: 'Logged In successfully',
                bgColor: '#b3cde0',
                loaderBg: '#005b96',              // Background color for toast
                textColor: '#000',
                // text color
                allowToastClose: true,       // Show the close button or not
                hideAfter: 4000,              // `false` to make it sticky or time in miliseconds to hide after
                position: 'top-right',
                icon: 'success'
              });
              if (apiResponse['output'].OrgDetails.length !== 0) {
              localStorage.setItem('E_InitiateCommunication',  '0');
              localStorage.setItem('searchkeyWord', 'null');
              localStorage.setItem('E_UserID', apiResponse['output']['UserID']);
              localStorage.setItem('E_UserName', apiResponse['output']['UserName']);
              localStorage.setItem('E_UserStatus',  apiResponse['output']['UserType']);
              if (apiResponse['output'].OrgDetails.length > 1) {
                localStorage.setItem('E_OrgName', 'Classtor Platform');
                localStorage.setItem('E_OrgId', apiResponse['output']['OrgDetails'][0].organization_id);
              } else {
                localStorage.setItem('E_OrgId', apiResponse['output']['OrgDetails'][0].organization_id);
                localStorage.setItem('E_OrgName', apiResponse['output']['OrgDetails'][0].organization_name);
              }
              this.successfullySignedIn.emit();
              } else {
                localStorage.setItem('E_InitiateCommunication',  '0');
                localStorage.setItem('searchkeyWord', 'null');
                localStorage.setItem('E_UserID', apiResponse['output']['UserID']);
                localStorage.setItem('E_UserName', apiResponse['output']['UserName']);
                localStorage.setItem('E_UserStatus',  apiResponse['output']['UserType']);
                this.registerOrg.emit();
              }
            }
          } else {
            const errorCode = apiResponse['code']['Code'];
            const errorDesc = [];
            const errorMessage = apiResponse['description'];
            for (const value in errorMessage) {
              errorDesc.push(errorMessage[value]);
            }
            this.errorModalHeading = 'Error';
            this.errorModalMessage = errorDesc[0];
            this.errorModalBtnText = '';
            this.errorModalImgStatus = true;
            this.errorModalStatus = true;
          }

        });
      }
    }
  }

  closeErrorModal() {
    this.errorModalStatus = false;
  }
}
