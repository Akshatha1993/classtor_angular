import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {HttpService} from '../../../core/http/http.service';
import {DataService} from '../../../core/services/data.service';

export interface Select {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-search-and-filters',
  templateUrl: './search-and-filters.component.html',
  styleUrls: ['./search-and-filters.component.css']
})
export class SearchAndFiltersComponent {

  @Output() askAQuestion = new EventEmitter();

  defaultSortType = 'TIME-ASCENDING';
  sortType: Select[] = [
    {value: 'UPVOTES', viewValue: 'Upvotes'},
    {value: 'TIME-ASCENDING', viewValue: 'Latest'},
    {value: 'TIME-DESCENDING', viewValue: 'Oldest'}
  ];

  defaultPostType = 'UNREAD';
  postType: Select[] = [
    {value: 'READ', viewValue: 'Read'},
    {value: 'UNREAD', viewValue: 'Unread'},
    {value: 'ALL', viewValue: 'All'}
  ];

  constructor(private httpService: HttpService,
              private dataService: DataService) {}

  emitAskAQuestionEvent() {
    this.askAQuestion.emit();
  }

  searchByKeyWord() {
    let values;
    let searchtext = [];
    values = document.getElementById('searchinput');
    if (values.value.length > 2) {
      if (/\s/.test(values) === true) {
        searchtext = values.value.toLowerCase().split(' ');
      }
      let socketid;
      socketid = [localStorage.getItem('E_UserID'), 'abc', 'def', 'ghk'];
      localStorage.setItem('searchkeyWord', searchtext.toString());
      this.httpService.getGroupMessages(localStorage.getItem('E_UserID'), '0', 0, 20, 'SEARCH', '', socketid.toString(), searchtext.toString()).subscribe(response => {
        this.dataService.setSearchKeyWord(true);
      });
    }
  }

  checktext() {
    let values;
    values = document.getElementById('searchinput');
    if (values.value.length < 3) {
      localStorage.setItem('searchkeyWord', 'null');
      this.dataService.setSearchKeyWord(true);
    }
  }

  readstatus(status) {
    this.dataService.setSearchKeyWord(status);
}
}
