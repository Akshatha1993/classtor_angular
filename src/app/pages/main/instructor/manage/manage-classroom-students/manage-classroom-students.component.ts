import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CategoryGroup, SelectableUser, Student} from '../../../../../Shared/interfaces';
import {MatSnackBar} from '@angular/material';
import {ChosenUsersService} from '../../../../../Shared/chosen-users.service';
import {Subscription} from 'rxjs';
import {ApiManagerService} from '../../../../../Shared/api-manager.service';
import {StudentClassroomManagementGetDataModal} from '../../../../../Modals/StudentClassroomManagement/StudentClassroomManagementGetDataModal';
import {HttpService} from '../../../../../core/http/http.service';
import {DataService} from '../../../../../core/services/data.service';

declare var $: any;
@Component({
  selector: 'app-manage-classroom-students',
  templateUrl: './manage-classroom-students.component.html',
  styleUrls: ['./manage-classroom-students.component.css']
})
export class ManageClassroomStudentsComponent implements OnInit {

  private apiCallSubscription: Subscription;

  @Input() classroomId: string;
  @Output() gotoHomePage = new EventEmitter();
  categoryName = 'Category not yet selected';
  currentCategoryId = '-2';
  categoryGroups: CategoryGroup[] = [];
  selectableStudentList;
  selectedStudentIdsList: string[];
  availableStudents = [];
  classList;
  selectedGroupIdToPost;
  errorMessageStatus = false;
  selectedStudentBackupList = [];
  changedUser = [];
  isChecked: boolean;
  openAddStudentStatus = false;
  selectedCategoryId;
  createdClassName;
  defaultGroupId;
  showCategoryFormStatus = false;
  showSubCategoryFormStatus = false;
  position;
  public demo1TabIndex = 0;

  // For the Error Modal.
  errorModalMessage;
  errorModalHeading;
  errorModalBtnText;
  errorModalImgStatus;
  errorModalStatus = false;

  constructor(private chosenStudents: ChosenUsersService,
              private snackBar: MatSnackBar,
              private apiManagerService: ApiManagerService,
              private httpService: HttpService,
              private dataService: DataService) {
    this.defaultGroupId = '';
    this.dataService.getcreatedClass.subscribe(response => {
      if (response !== '') {
        this.createdClassName = response;
      }
    });
  }

  ngOnInit() {
    // this.classList = JSON.parse(localStorage.getItem('E_ClassList'));
    this.getAllClassList();
    // this.chosenStudents.currentList.subscribe(updatedChosenStudentList => this.onStudentChosenChanged(updatedChosenStudentList));
    // this.onInitUpdateExistingStudentsToChosenStudentsService();
    this.getDataFromApiListenerAndPoster();

  }

  getAllClassList() {
    this.httpService.getAllClasses().subscribe(response => {
      let apiResponse;
      apiResponse = response;
      this.classList = apiResponse.response.result;
      if (this.createdClassName !== '') {
        for (let i = 0; i < this.classList.length; i++) {
          if (this.createdClassName === this.classList[i].group_name) {
            this.defaultGroupId = this.classList[i].group_id;
            this.selectedGroupIdToPost = this.classList[i].group_id;
          }
        }
      }
    });
  }

  getDataFromApiListenerAndPoster() {

    const api = this.apiManagerService.getManageClassroomData;
    const studentClassroomManagementGetData: StudentClassroomManagementGetDataModal = new StudentClassroomManagementGetDataModal(
      localStorage.getItem('E_OrgId'),
      '1'
    );
    this.apiManagerService.sendBControllerPostRequest(
      ManageClassroomStudentsComponent.name + '#GET', api,
      studentClassroomManagementGetData
    );

    this.apiCallSubscription = this.apiManagerService.onCallbackReceived.subscribe(
      (callback) => {

        if (callback.topic === (ManageClassroomStudentsComponent.name + '#GET')) {
          this.getDataFromApiReceived(callback.data);
        }

        if (callback.topic === (ManageClassroomStudentsComponent.name + '#POST')) {
          this.getDataFromApiReceived(callback.data);
        }
      }
    );
  }

  private getDataFromApiReceived(data: any) {
    this.categoryGroups = [];
    for (const row of data.response.result[0].result) {
      this.categoryGroups.push({
        categoryGroupID: row.organizational_category_group_id,
        categoryGroupName: row.organizational_category_group_name,
        category: []
      });
    }

    for (const row of data.response.result[1].result) {
      const indexOfParentCategoryGroup = this.getIndexOfCategoryGroupByCategoryGroupId(
        row.organizational_category_group_id,
        this.categoryGroups
      );
      this.categoryGroups[indexOfParentCategoryGroup].category.push({
        categoryId: row.organizational_category_id,
        categoryName: row.organizational_category_name
      });
    }

  }

  getIndexOfCategoryGroupByCategoryGroupId(searchElement: string, categoryGroups: CategoryGroup[]) {
    let index = -1;
    for (const categoryGroup of categoryGroups) {
      index += 1;
      if (categoryGroup.categoryGroupID === searchElement) {
        return index;
      }
    }
    return -1;
  }

  selectCategory(categoryId: string) {
    if (this.selectedGroupIdToPost === undefined) {
      this.errorMessageStatus = true;
    } else {
      this.errorMessageStatus = false;
      this.openAddStudentStatus = true;
      this.currentCategoryId = categoryId;
      this.getStudentsByCategory(categoryId);
      const tabCount = 2;
      this.demo1TabIndex = (this.demo1TabIndex + 1) % tabCount;
      this.categoryGroups.forEach(categoryGroup => {
        categoryGroup.category.forEach(singleCategory => {
          if (singleCategory.categoryId === categoryId) {
            this.categoryName = 'Showing ' + singleCategory.categoryName;
            return;
          }
        });
      });
    }
  }


  getStudentsByCategory(categoryId: string) {
    const selectableStudent: SelectableUser[] = [];
    this.selectedCategoryId = categoryId;
    let userList = [];
    if (categoryId === '-2') {
      this.snackBarCreator('Choose a category first.');
      return;
    }

    if (categoryId === '-1') {
      // Returning all Instructors
    }

    this.httpService.getStudentList(this.selectedGroupIdToPost, categoryId, '0').subscribe(response => {
      let apiResponse;
      // let selectableStudent;
      let user;
      user = {
        userid: '',
        status: ''
      };
      apiResponse = response;
      selectableStudent.push(apiResponse.response.result);
      this.selectableStudentList = selectableStudent[0];
      for (let i = 0; i < this.selectableStudentList.length; i++) {
        if (this.selectableStudentList[i].active === '1') {
          this.selectableStudentList[i].active = true;
        } else if (this.selectableStudentList[i].active === '0') {
          this.selectableStudentList[i].active = false;
        } else {
          this.selectableStudentList[i].active = false;
        }
        this.selectedStudentBackupList.push(this.selectableStudentList[i]);
      }
      // this.chosenStudents.updateChosenUserList(userList);
    });
  }

  onInitUpdateExistingStudentsToChosenStudentsService() {
    // This means get all students across all categories
    // let allStudents;
    let allStudents;
    this.getStudentsByCategory('-1');
    // for (const student of allStudents) {
    //   if (student.selected) {
    //     this.chosenStudents.addToChosenUserList(student.userId);
    //   }
    // }
  }

  selectAllStudentsOfCurrentCategoryId() {

    this.changedUser = [];
    for (let i = 0; i < this.selectedStudentBackupList.length; i++) {
      if (this.selectedStudentBackupList[i].active === false) {
        this.selectableStudentList[i].active = true;
        this.changedUser.push({userId: this.selectedStudentBackupList[i].UserID, userStatus: true});
      }
    }
  }

  unselectStudentsOfCurrentGroupId() {
    this.changedUser = [];
    for (let i = 0; i < this.selectedStudentBackupList.length; i++) {
      if (this.selectedStudentBackupList[i].active === true) {
        this.selectableStudentList[i].active = false;
        this.changedUser.push({userId: this.selectedStudentBackupList[i].UserID, userStatus: false});
      }
    }
  }

  onSelectionChangedGroup($event) {
    this.selectedGroupIdToPost = $event.value;
  }

  updateSelectedStudents() {
    // todo api call => use selectedUserIdsList to remove every students connection and update with new list.
    if (this.selectedGroupIdToPost === undefined) {
      this.errorMessageStatus = true;
    } else {
      let userList = '';
      for (let i = 0; i < this.changedUser.length; i++) {
        if (this.changedUser[i]['userStatus'] === true || this.changedUser[i]['userStatus'] === '1') {
          this.changedUser[i]['userStatus'] = '1';
        } else {
          this.changedUser[i]['userStatus'] = '0';
        }
        userList = userList + '{' + '"' + this.changedUser[i]['userId'] + '"' + ':' + this.changedUser[i]['userStatus'] + '}' + ',';
        // changedStatusUser.push(userList);
      }
      userList = userList.substring(0, userList.length - 1);
      userList = '[' + userList + ']';
      this.httpService.addStudentsToClass(this.selectedGroupIdToPost, userList, '0').subscribe(response => {
        let apiResponse;
        apiResponse = response;
        apiResponse = apiResponse.response.result;
        if (apiResponse['status'] === 1) {
          this.changedUser = [];
          this.snackBarCreator('Student management updated successfully');
          this.gotoHomePage.emit();
        }
      });
    }
  }

  //
  selectUser(userId, isChecked: boolean) {
    if (isChecked === true) {
      if (this.changedUser.length === 0) {
        for (let i = 0; i < this.selectedStudentBackupList.length; i++) {
          if (userId === this.selectedStudentBackupList[i].UserID) {
            this.selectableStudentList[i].active = true;
            this.changedUser.push({userId: this.selectedStudentBackupList[i].UserID, userStatus: false});
          }
        }
      } else {
        for (let i = 0; i < this.changedUser.length; i++) {
          const indexToRemove = this.changedUser.findIndex(x => x['userId'] === userId);
          if (indexToRemove >= 0) {
            this.changedUser[indexToRemove]['userStatus'] = false;
          } else {
            for (let i = 0; i < this.selectedStudentBackupList.length; i++) {
              if (userId === this.selectedStudentBackupList[i].UserID) {
                this.selectableStudentList[i].active = true;
                this.changedUser.push({userId: this.selectedStudentBackupList[i].UserID, userStatus: false});
              }
            }
          }
        }
      }
    } else {
      if (this.changedUser.length === 0) {
        for (let i = 0; i < this.selectedStudentBackupList.length; i++) {
          if (userId === this.selectedStudentBackupList[i].UserID) {
            this.selectableStudentList[i].active = false;
            this.changedUser.push({userId: this.selectedStudentBackupList[i].UserID, userStatus: true});
          }
        }
      } else {
        for (let i = 0; i < this.changedUser.length; i++) {
          const indexToRemove = this.changedUser.findIndex(x => x['userId'] === userId);
          if (indexToRemove >= 0) {
            this.changedUser[indexToRemove]['userStatus'] = true;
          } else {
            for (let i = 0; i < this.selectedStudentBackupList.length; i++) {
              if (userId === this.selectedStudentBackupList[i].UserID) {
                this.selectableStudentList[i].active = false;
                this.changedUser.push({userId: this.selectedStudentBackupList[i].UserID, userStatus: true});
              }
            }
          }
        }
      }
    }
  }

  snackBarCreator(message: string) {
    this.snackBar.open(message, 'Close', {
      duration: 1000,
    });
  }

  addStudent(username, email) {
    if (this.selectedGroupIdToPost === undefined) {
      this.errorMessageStatus = true;
    } else if (localStorage.getItem('E_OrgId') === null) {
      this.errorModalHeading = 'Error';
      this.errorModalMessage = 'Please Register Organization before adding students';
      this.errorModalBtnText = '';
      this.errorModalImgStatus = true;
      this.errorModalStatus = true;
    } else {
      this.httpService.addStudentByAdmin(username.viewModel, email.viewModel, '0', this.selectedGroupIdToPost, this.selectedCategoryId).subscribe(response => {
        let apiResponse;
        apiResponse = response;
        apiResponse = apiResponse.response.result;
        if (apiResponse['status'] === 1) {
          this.snackBarCreator('Student is added successfully');
          let name;
          let mail;
          name = document.getElementById('username');
          name.value = '';
          mail = document.getElementById('email');
          mail.value = '';
          this.getStudentsByCategory(this.selectedCategoryId);
        } else {
          const errorCode = apiResponse['code']['Code'];
          const errorDesc = [];
          const errorMessage = apiResponse['description'];
          for (const value in errorMessage) {
            errorDesc.push(errorMessage[value]);
          }
          this.errorModalHeading = 'Error';
          this.errorModalMessage = errorDesc[0];
          this.errorModalBtnText = '';
          this.errorModalImgStatus = true;
          this.errorModalStatus = true;
        }
      });
    }
  }

  closeErrorModal() {
    this.errorModalStatus = false;
  }

  addCategory() {
    this.showCategoryFormStatus = true;
  }

  addSubCategory(i) {
    this.position = i;
    this.showSubCategoryFormStatus = true;
  }

  submitAddCategory(categoryname) {
  if (localStorage.getItem('E_OrgId') === null) {
      this.errorModalHeading = 'Error';
      this.errorModalMessage = 'Please Register Organization before adding Category';
      this.errorModalBtnText = '';
      this.errorModalImgStatus = true;
      this.errorModalStatus = true;
    } else {
      this.httpService.addOrgGroupCategory(categoryname.viewModel).subscribe(response => {
      let apiResponse;
      apiResponse = response;
      apiResponse = apiResponse.response.result;
      if (apiResponse['status'] === 1) {
        this.categoryGroups.push({
          categoryGroupID: apiResponse.output.OrgCatGrpID.toString(),
          categoryGroupName: apiResponse.output.OrgCatGrpName,
          category: []
        });
        this.snackBarCreator('Category is added successfully');
        this.showCategoryFormStatus = false;
      } else {
        const errorCode = apiResponse['code']['Code'];
        const errorDesc = [];
        const errorMessage = apiResponse['description'];
        for (const value in errorMessage) {
          errorDesc.push(errorMessage[value]);
        }
        this.errorModalHeading = 'Error';
        this.errorModalMessage = errorDesc[0];
        this.errorModalBtnText = '';
        this.errorModalImgStatus = true;
        this.errorModalStatus = true;
      }
    });
  }
  }

  submitAddSUbCategory(categoryid, subcategoryname) {
    if (localStorage.getItem('E_OrgId') === null) {
      this.errorModalHeading = 'Error';
      this.errorModalMessage = 'Please Register Organization before adding Sub-Category';
      this.errorModalBtnText = '';
      this.errorModalImgStatus = true;
      this.errorModalStatus = true;
    } else {
      this.httpService.addOrgSubGroupCategory(categoryid, subcategoryname.viewModel).subscribe(response => {
        let apiResponse;
        apiResponse = response;
        apiResponse = apiResponse.response.result;
        if (apiResponse['status'] === 1) {
          const indexOfParentCategoryGroup = this.getIndexOfCategoryGroupByCategoryGroupId(categoryid, this.categoryGroups);
          this.categoryGroups[indexOfParentCategoryGroup].category.push({
            categoryId: apiResponse.output.OrgCatID.toString(),
            categoryName: apiResponse.output.OrgCatName
          });
          this.snackBarCreator('Sub-Category is added successfully');
          this.showSubCategoryFormStatus = false;
        } else {
          const errorCode = apiResponse['code']['Code'];
          const errorDesc = [];
          const errorMessage = apiResponse['description'];
          for (const value in errorMessage) {
            errorDesc.push(errorMessage[value]);
          }
          this.errorModalHeading = 'Error';
          this.errorModalMessage = errorDesc[0];
          this.errorModalBtnText = '';
          this.errorModalImgStatus = true;
          this.errorModalStatus = true;
        }
      });
    }
  }
}
