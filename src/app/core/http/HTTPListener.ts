import {HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {catchError, finalize, map} from 'rxjs/operators';
import {ObjectMapper} from 'json-object-mapper';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {NgxSpinnerService} from 'ngx-spinner';
import {Injectable} from '@angular/core';

@Injectable()
export class HttpStatusService {}

@Injectable()
export class HTTPListener implements HttpInterceptor {

  constructor(private httpStatus: HttpStatusService,
              private http: HttpClient,
              private spinner: NgxSpinnerService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      map(event => {
        this.spinner.show();
        return event;
      }),
      catchError(error => {
        console.log('ERROR ' + error);
        if (error.status === 401 || error.status === 400) {
          // do waht ever you want if the token is expired.
        }
        return Observable.throw(error);
      }),
      finalize(() => {
        // this.httpStatus.setStatus(false);
        this.spinner.hide();
      })
    );
  }
}
