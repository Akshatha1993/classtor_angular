import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {AClass} from '../../Shared/interfaces';
import {DataService} from '../../core/services/data.service';
import {HttpService} from '../../core/http/http.service';
import {Observable} from 'rxjs';
import {BreakpointObserver, Breakpoints, BreakpointState} from '@angular/cdk/layout';
import {map} from 'rxjs/operators';
import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  providers: [DatePipe]
})
export class SidebarComponent implements OnInit {

  @Output() WantToCreateANewClass = new EventEmitter();
  @Output() closeNav = new EventEmitter();
  @Output() EnterClass = new EventEmitter<string>();
  @Output() ShowDetails = new EventEmitter();
  socketIds = [];
  userName;

  @ViewChild('drawer', {static: true}) drawer: any;
  public isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map((result: BreakpointState) => result.matches));

  classesList;
  emittedClass;
  dataServiceMapArray;
  unreadCount;
  userStatus;
  constructor(private snackBar: MatSnackBar,
              private dataService: DataService,
              private httpService: HttpService,
              private breakpointObserver: BreakpointObserver,
              private datePipe: DatePipe) {
    this.dataService.getrefreshCommunication.subscribe(response => {
      let dataServiceResponse;

      if (response !== '') {
        dataServiceResponse = response;
        if (dataServiceResponse === true) {
          this.getAllClassGroups();
        }
      }
    });


    // this.dataService.getMapArrayToMessage.subscribe(response => {
    //   let askQuestionMapArray;
    //   let count = [];
    //   askQuestionMapArray = response;
    //   // this.searchResult = false;
    //   if (askQuestionMapArray !== '') {
    //     if (askQuestionMapArray.get(localStorage.getItem('E_classId')).get('0') !== undefined) {
    //       this.dataServiceMapArray = askQuestionMapArray.get(localStorage.getItem('E_classId')).get('0');
    //       for (let i = 0; i < this.classesList.length; i++) {
    //         this.classesList[i]['count'] = [];
    //         for (let j = 0; j < this.dataServiceMapArray.length; j++) {
    //           if (this.dataServiceMapArray[j].message.category === this.classesList[i].group_key) {
    //             if (this.dataServiceMapArray[j].message.sender_id !== localStorage.getItem('E_UserID')) {
    //               if (this.dataServiceMapArray[j].read_status === 0) {
    //                 this.classesList[i]['count'].push(this.dataServiceMapArray[j]);
    //                 this.classesList[i]['readcount'] = this.classesList[i]['count'].length;
    //               }
    //             }
    //           }
    //         }
    //       }
    //     }
    //   }
    //     });
  }

ngOnInit() {
  // this.getGroupData();
  this.userName = localStorage.getItem('E_UserName');
  this.userStatus = localStorage.getItem('E_UserStatus');
  this.getAllClassGroups();
  this.dataService.setCategoryMessages(true);
}


  getAllClassGroups() {
  let postDate;
    this.socketIds = [];
    this.httpService.getAllClasses().subscribe(response => {
      let apiResponse;
      apiResponse = response;
      this.classesList = apiResponse.response.result;
      localStorage.setItem('E_ClassList', JSON.stringify(this.classesList));
      for (let i = 0; i < this.classesList.length; i++) {
        postDate = this.classesList[i].date_time_created;
        this.classesList[i].date_time_created = moment(postDate).format('DD MMM YYYY');
        this.socketIds.push(this.classesList[i].group_key);
      }
      this.socketIds.unshift(localStorage.getItem('E_UserID'));
      localStorage.setItem('E_socketIdConnection', JSON.stringify(this.socketIds));
      localStorage.setItem('E_classId', this.socketIds[1]);
      localStorage.setItem('E_UserClassType', this.classesList[0].user_type_id);
      this.dataService.setInitiateCommunication('0');

    });
  }

  openSnackBar(groupName: string) {
    this.snackBar.open('Entering ' + groupName, 'Close', {
      duration: 2000,
    });
  }

  emitWantToCreateANewClass() {
    this.closeNav.emit();
    this.WantToCreateANewClass.emit();
  }

  emitEnterClass(i, classKey: string, usertype) {
    this.emittedClass = i;
    this.closeNav.emit();
    localStorage.setItem('E_classId', classKey);
    localStorage.setItem('E_UserClassType', usertype);
    this.dataService.setCategoryMessages(true);
    this.EnterClass.emit(classKey);
}

  getGroupData() {
    let socketid;
    socketid = [localStorage.getItem('E_UserID'), 'abc', 'def', 'ghk'];
    this.httpService.getGroupMessages(localStorage.getItem('E_UserID'), '0', 0, 9, 'PARENTID', localStorage.getItem('E_classId'), socketid.toString(), '').subscribe(response => {
      let result;
      result = response;
    });
  }
  showDetails() {
    this.closeNav.emit();
    this.ShowDetails.emit();
    
  }
}

