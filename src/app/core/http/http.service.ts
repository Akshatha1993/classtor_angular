import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DataService} from '../services/data.service';
import {Observable, throwError} from 'rxjs';
import {ObjectMapper} from 'json-object-mapper';
import {catchError} from 'rxjs/operators';
import {
  PROJECT_GETINITIALDATAS_PATH,
  PROJECT_SENDMESSAGE_PATH, PROJECT_UPDATEREADSTATUS_PATH,
  PROJECT_GETGROUPDATAS_PATH, PROJECT_SENDATTRIBUTE_PATH, PROJECT_DELETE_MESSAGE_PATH,
  PROJECT_FETCHPOINTS_PATH, PROJECT_LOGIX_PATH, PROJECT_API_PATH, PROJECT_ID,
  PROJECT_QUEIT
} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  public getGroupMessages(userid, parentid, fromlimit, tolimit, datatype, categories, userlist, searchkey) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      user_id: userid,
      id: parentid,
      project_id: PROJECT_ID,
      from_limit: fromlimit,
      to_limit: tolimit,
      type: datatype,
      category: categories,
      contraList: '',
      userList: userlist,
      searchKey: searchkey
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;

    return this.http
      .post(PROJECT_GETGROUPDATAS_PATH, stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public getInitialMessage(userid, parentid, fromlimit, tolimit, datatype) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      user_id: userid,
      id: parentid,
      project_id: PROJECT_ID,
      from_limit: fromlimit,
      to_limit: tolimit,
      type: datatype,
      category: ''
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;

    return this.http
      .post(PROJECT_GETINITIALDATAS_PATH, stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public addAttribute(object) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const stringObject: string = ObjectMapper.serialize(object) as string;

    return this.http
      .post(PROJECT_SENDATTRIBUTE_PATH, stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public sendMessage(object) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_SENDMESSAGE_PATH, stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public deleteMessages(uniqueid, activate) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      unique_id: uniqueid,
      project_id: PROJECT_ID,
      active: activate
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;

    return this.http
      .post(PROJECT_DELETE_MESSAGE_PATH,
        stringObject, options)
      .pipe(catchError(this.handleError));
  }
  

  public userChangePassword(currentpassword, newpassword, retypepassword) {
  const options = {headers: {'Content-Type': 'application/json'}};
  const object = {
    UserId: localStorage.getItem('E_UserID'),
    OldPassword: currentpassword,
    NewPassword: newpassword
  };
  const stringObject: string = ObjectMapper.serialize(object) as string;

  return this.http
    .post(PROJECT_LOGIX_PATH + '/UsersM/88364/ChangePassword.php',
      stringObject, options)
    .pipe(catchError(this.handleError));
}


  public getClassList() {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      UserID: localStorage.getItem('E_UserID'),
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;

    return this.http
      .post(PROJECT_API_PATH + '/Get_group_Details_for_User.php?',
        stringObject, options)
      .pipe(catchError(this.handleError));
  }


  public getUserDetails() {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      UserID: localStorage.getItem('E_UserID'),
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;

    return this.http
      .post(PROJECT_API_PATH + '/fetch_User_Details_by_UserID.php?',
        stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public fetchSenderPoints() {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      sender_id: localStorage.getItem('E_UserID'),
      project_id: PROJECT_ID
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_FETCHPOINTS_PATH,
        stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public userRegistration(username, email, password) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      UserName: username,
      EmailID: email,
      Password: password,
      UserTypeID: '2'
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_LOGIX_PATH + '/UsersM/88364/RegisterUser.php', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public confirmPasscode(email, passcode) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      EmailID: email,
      Passcode: passcode
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_LOGIX_PATH + '/UsersM/88364/ConfirmUser.php?', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public userLogin(email, password) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      EmailID: email,
      Password: password
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_LOGIX_PATH + '/UsersM/88364/UserLogin.php?', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public forgotPassword(email) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      EmailID: email,
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_LOGIX_PATH + '/UsersM/88364/ForgotPassword.php', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public userResetPassword(email, passcode, password) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      EmailID: email,
      Passcode: passcode,
      Password: password
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_LOGIX_PATH + '/UsersM/88364/ResetPassword.php', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public createClassRoom(classname, classcode, classdescription, usertypeid) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      group_name: classname,
      group_code: classcode,
      group_description: classdescription,
      user_id: localStorage.getItem('E_UserID'),
      user_type_id: usertypeid
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_LOGIX_PATH + '/Classroom/c4ad7/CreateClassroom.php', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public getAllClasses() {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      UserID: localStorage.getItem('E_UserID'),
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_API_PATH + '/Get_group_Details_for_User.php?', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public getStudentList(groupid, categoryid, usertype) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      GroupID: groupid,
      OrgCatGroupID: categoryid,
      UserType: usertype
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_API_PATH + '/User_details_from_organizational_categor.php', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public addStudentsToClass(groupid, userlist, usertype) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      group_id: groupid,
      UserList: userlist,
      user_type_id: usertype
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_LOGIX_PATH + '/Classroom/c4ad7/AddMutipleUserToClass.php', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public addStudentByAdmin(username, email, usertype, groupid, categoryid) {

    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      UserName: username,
      EmailID: email,
      UserTypeID: usertype,
      GroupID: groupid,
      OrgID: localStorage.getItem('E_OrgId'),
      OrgCatID: categoryid
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_LOGIX_PATH + '/UsersM/88364/RegisterUserByAdmin.php', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public organisationDetail(orgname, orguniquecode, usertype) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      OrgName: orgname,
      UniqueCode: orguniquecode,
      UserID: localStorage.getItem('E_UserID'),
      UserTypeID: usertype
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_LOGIX_PATH + '/UsersM/88364/RegisterOrgForUser.php', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public addOrgGroupCategory(categoryname) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      OrgID: localStorage.getItem('E_OrgId'),
      OrgCatGrpName: categoryname,

    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_LOGIX_PATH + '/UsersM/88364/RegisterOrgCatGroup.php', stringObject, options)
      .pipe(catchError(this.handleError));
  }


  public addOrgSubGroupCategory(categoryId, subcategoryname) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      OrgID: localStorage.getItem('E_OrgId'),
      OrgCatGrpID: categoryId,
      OrgCatName: subcategoryname,

    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_LOGIX_PATH + '/UsersM/88364/RegisterOrgCat.php', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public resendPasscode(email) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      EmailID: email
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_LOGIX_PATH + '/UsersM/88364/ResendPasscode.php?', stringObject, options)
      .pipe(catchError(this.handleError));
  }
  public addTag(tagName,groupid) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      GroupID: groupid,
      TagName:tagName
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_API_PATH + '/insert_into_tag_m.php?', stringObject, options)
      .pipe(catchError(this.handleError));
  }
  public fetchTag(groupid) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      group_id:groupid
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_API_PATH + '/fetch_tag_m_by_group_id.php?', stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public updateReadStatus(receiverid, uniqurids) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      sender_id: receiverid,
      receiver_id: localStorage.getItem('E_UserID'),
      unique_id: uniqurids,
      receiver_type: 'I',
      message_type: 'ACK',
      read_status: 1
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;

    return this.http
      .post(PROJECT_UPDATEREADSTATUS_PATH, stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public tutorMessage(classid, classname, teacherid, teachername, orgid, orgname) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = [{
      api: PROJECT_LOGIX_PATH + '/Classroom/c4ad7/InstituteMail.php',
      data: {
        ClassID: classid,
        ClassName: classname,
        TeacherID: teacherid,
        TeacherName: teachername,
        InstituteID: orgid,
        InstituteName: orgname
      },
      projectID: PROJECT_ID,
      tag: 'User'
    }];
    const stringObject: string = ObjectMapper.serialize(object) as string;

    return this.http
      .post(PROJECT_QUEIT, stringObject, options)
      .pipe(catchError(this.handleError));
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    // window.alert(error.error.details);
    return throwError(error);
  }
}
