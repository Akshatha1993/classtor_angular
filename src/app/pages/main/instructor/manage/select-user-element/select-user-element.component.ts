import {Component, Input, OnInit} from '@angular/core';
import {ChosenUsersService} from '../../../../../Shared/chosen-users.service';

@Component({
  selector: 'app-select-user-element',
  templateUrl: './select-user-element.component.html',
  styleUrls: ['./select-user-element.component.css']
})

export class SelectUserElementComponent implements OnInit {

  @Input() isChecked: boolean;
  @Input() userId: string;
  @Input() userName: string;
  @Input() userProfilePicture: string;
  selectedUserIdsList: string[];
  temp: string;

  constructor(private chosenUsers: ChosenUsersService) { }

  ngOnInit() {
    this.chosenUsers.currentList.subscribe(updatedChosenUserList => this.chosenUsersChanged(updatedChosenUserList));
  }

  selectUser(isChecked: boolean) {
    if (!isChecked) {
      this.chosenUsers.removeFromChosenUserList(this.userId, false);
    } else {
      this.chosenUsers.addToChosenUserList(this.userId, true);
    }
  }

  chosenUsersChanged(updatedChosenUserList: string[]) {
    this.temp = '';
    this.selectedUserIdsList = updatedChosenUserList;
    // this.selectedUserIdsList.forEach(value => this.temp += '|' + value);
    // for (let i = 0; i < this.selectedUserIdsList.length; i++) {
    //   if (this.selectedUserIdsList[i]['userStatus'] === true) {
    //     this.isChecked = true;
    //   } else {
    //     this.isChecked = false;
    //   }
    // }
  }
}
