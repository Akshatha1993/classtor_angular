import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CategoryGroup, SelectableUser} from '../../../../../../Shared/interfaces';
import {MatSnackBar} from '@angular/material';
import {ChosenUsersService} from '../../../../../../Shared/chosen-users.service';
import {StudentClassroomManagementGetDataModal} from '../../../../../../Modals/StudentClassroomManagement/StudentClassroomManagementGetDataModal';
import {ApiManagerService} from '../../../../../../Shared/api-manager.service';
import {Subscription} from 'rxjs';
import {HttpService} from "../../../../../../core/http/http.service";

@Component({
  selector: 'app-manage-classroom-instructors',
  templateUrl: './manage-classroom-instructors.component.html',
  styleUrls: ['./manage-classroom-instructors.component.css']
})
export class ManageClassroomInstructorsComponent implements OnInit {

  private apiCallSubscription: Subscription;
  @Input() classroomId: string;
  @Output() gotoHomePage = new EventEmitter();
  categoryName = 'Category not yet selected';
  allInstructorNames: string[];
  currentCategoryId = '-2';
  categoryGroups = [];
  selectableInstructorList;
  selectedInstructorIdsList: string[];
  availableStudents = [];
  classList;
  selectedGroupIdToPost;
  errorMessageStatus = false;
  selectedStudentBackupList = [];
  changedUser = [];
  selectedCategoryId;
  openAddStudentStatus = false;
  showCategoryFormStatus = false;
  showSubCategoryFormStatus = false;
  position;
  public demo1TabIndex = 0;

  // For the Error Modal.
  errorModalMessage;
  errorModalHeading;
  errorModalBtnText;
  errorModalImgStatus;
  errorModalStatus = false;

  constructor(private chosenInstructors: ChosenUsersService, private snackBar: MatSnackBar,
              private apiManagerService: ApiManagerService,
              private httpService: HttpService) {
  }

  ngOnInit() {
    this.classList = JSON.parse(localStorage.getItem('E_ClassList'));
    this.getInstructorCategories();
    this.chosenInstructors.currentList.subscribe(updatedChosenInstructorsList =>
      this.onInstructorChosenChanged(updatedChosenInstructorsList)
    );
    // this.allInstructorNames = this.getAllInstructorNames();
    this.onInitUpdateExistingInstructorsToChosenInstructorsService();
  }

  onInstructorChosenChanged(updatedChosenInstructorList: string[]) {
    this.changedUser = [];
    this.selectedInstructorIdsList = updatedChosenInstructorList;
    if (this.selectedInstructorIdsList.length > 0) {
      for (let i = 0; i < this.selectedInstructorIdsList.length; i++) {
        if (this.selectedInstructorIdsList[i]['userStatus'] === true) {
          const index =  this.selectedStudentBackupList.findIndex(x => x['UserID'] === this.selectedInstructorIdsList[i]['userId']);
          if (this.selectedStudentBackupList[index].active === true) {
          } else {
            this.changedUser.push(this.selectedInstructorIdsList[i]);
          }
        } else {
          this.changedUser.push(this.selectedInstructorIdsList[i]);
        }
      }
    }
  }

  selectCategory(categoryId: string) {
    if (this.selectedGroupIdToPost === undefined) {
      this.errorMessageStatus = true;
    } else {
      this.errorMessageStatus = false;
      this.openAddStudentStatus = true;
      this.currentCategoryId = categoryId;
      this.selectableInstructorList = this.getInstructorsByCategory(categoryId);
      const tabCount = 2;
      this.demo1TabIndex = (this.demo1TabIndex + 1) % tabCount;
      this.categoryGroups.forEach(categoryGroup => {
        categoryGroup.category.forEach(singleCategory => {
          if (singleCategory.categoryId === categoryId) {
            this.categoryName = 'Showing ' + singleCategory.categoryName;
            return;
          }
        });
      });
    }
  }

  getInstructorCategories() {
    // TODO API CALL HERE
    const api = this.apiManagerService.getManageClassroomData;
    const studentClassroomManagementGetData: StudentClassroomManagementGetDataModal = new StudentClassroomManagementGetDataModal(
      localStorage.getItem('E_OrgId'),
      '1'
    );
    this.apiManagerService.sendBControllerPostRequest(
      ManageClassroomInstructorsComponent.name + '#GET', api,
      studentClassroomManagementGetData
    );

    this.apiCallSubscription = this.apiManagerService.onCallbackReceived.subscribe(
      (callback) => {

        if (callback.topic === (ManageClassroomInstructorsComponent.name + '#GET')) {
          this.getDataFromApiReceived(callback.data);
        }

        if (callback.topic === (ManageClassroomInstructorsComponent.name + '#POST')) {
          this.getDataFromApiReceived(callback.data);
        }
      }
    );
  }

  private getDataFromApiReceived(data: any) {

    for (const row of data.response.result[0].result) {
      this.categoryGroups.push({
        categoryGroupID: row.organizational_category_group_id,
        categoryGroupName: row.organizational_category_group_name,
        category: []
      });
    }

    for (const row of data.response.result[1].result) {
      const indexOfParentCategoryGroup = this.getIndexOfCategoryGroupByCategoryGroupId(
        row.organizational_category_group_id,
        this.categoryGroups
      );
      this.categoryGroups[indexOfParentCategoryGroup].category.push({
        categoryId: row.organizational_category_id,
        categoryName: row.organizational_category_name
      });
    }

    for (const row of data.response.result[2].result) {
      this.availableStudents.push({
        firstName: row.first_name,
        lastName: row.last_name,
        userId: row.user_id,
        organizational_category_id: row.organizational_category_id,
        group_id: row.group_id,
        active: row.active,
        temporary_ban: row.temporary_ban
      });
    }

  }

  getIndexOfCategoryGroupByCategoryGroupId(searchElement: string, categoryGroups: CategoryGroup[]) {
    let index = -1;
    for (const categoryGroup of categoryGroups) {
      index += 1;
      if (categoryGroup.categoryGroupID === searchElement) {
        return index;
      }
    }
    return -1;
  }

  // getAllInstructorNames() {
  //   const allElements: string[] = [];
  //   for (const element of this.getInstructorsByCategory('-1')) {
  //     allElements.push(element.fullName);
  //   }
  //   return allElements;
  // }

  getInstructorsByCategory(categoryId: string) {
    this.selectedCategoryId = categoryId;
    if (categoryId === '-2') {
      this.snackBarCreator('Choose a category first.');
      return;
    }

    if (categoryId === '-1') {
      // Returning all Instructors
    }

    // TODO API CALL here and dynamic programming for efficiency. Highly inefficient now.
    const selectableInstructor: SelectableUser[] = [];

    this.httpService.getStudentList(this.selectedGroupIdToPost, categoryId, '1').subscribe(response => {
      let apiResponse;
      // let selectableStudent;
      apiResponse = response;
      selectableInstructor.push(apiResponse.response.result);
      this.selectableInstructorList = selectableInstructor[0];
      for (let i = 0; i < this.selectableInstructorList.length; i++) {
        if (this.selectableInstructorList[i].active === '1') {
          this.selectableInstructorList[i].active = true;
        } else if (this.selectableInstructorList[i].active === '0') {
          this.selectableInstructorList[i].active = false;
        } else {
          this.selectableInstructorList[i].active = false;
        }
        this.selectedStudentBackupList.push(this.selectableInstructorList[i]);
      }
    });
  }

  onInitUpdateExistingInstructorsToChosenInstructorsService() {
    // This means get all Instructors across all categories
    // const allInstructors: SelectableUser[] =
      this.getInstructorsByCategory('-1');
    // for (const Instructor of allInstructors) {
    //   if (Instructor.selected) {
    //     this.chosenInstructors.addToChosenUserList(Instructor.userId);
    //   }
    // }
  }


  selectAllInstructorsOfCurrentCategoryId() {
    this.changedUser = [];
    for (let i = 0; i < this.selectedStudentBackupList.length; i++) {
      if (this.selectedStudentBackupList[i].active === false) {
        this.selectableInstructorList[i].active = true;
        this.changedUser.push({userId: this.selectedStudentBackupList[i].UserID, userStatus: true});
      }
    }
  }

  unselectInstructorsOfCurrentGroupId() {
    this.changedUser = [];
    for (let i = 0; i < this.selectedStudentBackupList.length; i++) {
      if (this.selectedStudentBackupList[i].active === true) {
        this.selectableInstructorList[i].active = false;
        this.changedUser.push({userId: this.selectedStudentBackupList[i].UserID, userStatus: false});
      }
    }
  }

  onSelectionChangedGroup($event) {
    this.selectedGroupIdToPost = $event.value;
  }

  selectUser(userId, isChecked: boolean) {
    if (isChecked === true) {
      if (this.changedUser.length === 0) {
        for (let i = 0; i < this.selectedStudentBackupList.length; i++) {
          if (userId === this.selectedStudentBackupList[i].UserID) {
            this.selectableInstructorList[i].active = true;
            this.changedUser.push({userId: this.selectedStudentBackupList[i].UserID, userStatus: false});
          }
        }
      } else {
        for (let i = 0; i < this.changedUser.length; i++) {
          const indexToRemove = this.changedUser.findIndex(x => x['userId'] === userId);
          if (indexToRemove >= 0) {
            this.changedUser[indexToRemove]['userStatus'] = false;
          } else {
            for (let i = 0; i < this.selectedStudentBackupList.length; i++) {
              if (userId === this.selectedStudentBackupList[i].UserID) {
                this.selectableInstructorList[i].active = true;
                this.changedUser.push({userId: this.selectedStudentBackupList[i].UserID, userStatus: false});
              }
            }
          }
        }
      }
    } else {
      if (this.changedUser.length === 0) {
        for (let i = 0; i < this.selectedStudentBackupList.length; i++) {
          if (userId === this.selectedStudentBackupList[i].UserID) {
            this.selectableInstructorList[i].active = false;
            this.changedUser.push({userId: this.selectedStudentBackupList[i].UserID, userStatus: true});
          }
        }
      } else {
        for (let i = 0; i < this.changedUser.length; i++) {
          const indexToRemove = this.changedUser.findIndex(x => x['userId'] === userId);
          if (indexToRemove >= 0) {
            this.changedUser[indexToRemove]['userStatus'] = true;
          } else {
            for (let i = 0; i < this.selectedStudentBackupList.length; i++) {
              if (userId === this.selectedStudentBackupList[i].UserID) {
                this.selectableInstructorList[i].active = false;
                this.changedUser.push({userId: this.selectedStudentBackupList[i].UserID, userStatus: true});
              }
            }
          }
        }
      }
    }
  }

  updateSelectedInstructors() {
    // todo api call => use selectedInstructorIdsList to remove every Instructors connection and update with new list.
    if (this.selectedGroupIdToPost === undefined) {
      this.errorMessageStatus = true;
    } else {
      let userList = '';
      for (let i = 0; i <  this.changedUser.length; i++) {
        if (this.changedUser[i]['userStatus'] === true) {
          this.changedUser[i]['userStatus'] = '1';
        } else {
          this.changedUser[i]['userStatus'] = '0';
        }
        userList = userList + '{' + '"' + this.changedUser[i]['userId'] + '"' + ':' + this.changedUser[i]['userStatus'] + '}' + ',';
        // changedStatusUser.push(userList);
      }
      userList = userList.substring(0, userList.length - 1);
      userList = '[' + userList + ']';
      this.httpService.addStudentsToClass(this.selectedGroupIdToPost, userList, '0').subscribe(response => {
        let apiResponse;
        apiResponse = response;
        apiResponse = apiResponse.response.result;
        if (apiResponse['status'] === 1) {
          this.snackBarCreator('Student management updated successfully');
          this.gotoHomePage.emit();
        }
      });
    }
  }

  makeString(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  makeId(length) {
    let result = '';
    const characters = '0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  snackBarCreator(message: string) {
    this.snackBar.open(message, 'Close', {
      duration: 1000,
    });
  }

  addInstructor(username, email) {
    if (this.selectedGroupIdToPost === undefined) {
      this.errorMessageStatus = true;
    } else if (localStorage.getItem('E_OrgId') === null) {
      this.errorModalHeading = 'Error';
      this.errorModalMessage = 'Please Register Organization before adding Instructor';
      this.errorModalBtnText = '';
      this.errorModalImgStatus = true;
      this.errorModalStatus = true;
    } else {
      this.httpService.addStudentByAdmin(username.viewModel, email.viewModel, '1', this.selectedGroupIdToPost, this.selectedCategoryId).subscribe(response => {
        let apiResponse;
        apiResponse = response;
        apiResponse = apiResponse.response.result;
        if (apiResponse['status'] === 1) {
          this.snackBarCreator('Student is added successfully');
          let name;
          let mail;
          name = document.getElementById('username');
          name.value = '';
          mail = document.getElementById('email');
          mail.value = '';
          this.getInstructorsByCategory(this.selectedCategoryId);
        } else {
          const errorCode = apiResponse['code']['Code'];
          const errorDesc = [];
          const errorMessage = apiResponse['description'];
          for (const value in errorMessage) {
            errorDesc.push(errorMessage[value]);
          }
          this.errorModalHeading = 'Error';
          this.errorModalMessage = errorDesc[0];
          this.errorModalBtnText = '';
          this.errorModalImgStatus = true;
          this.errorModalStatus = true;
        }
      });
    }
  }

  addCategory() {
    this.showCategoryFormStatus = true;
  }

  addSubCategory(i) {
    this.position = i;
    this.showSubCategoryFormStatus = true;
  }

  submitAddCategory(categoryname) {
    if (localStorage.getItem('E_OrgId') === null) {
      this.errorModalHeading = 'Error';
      this.errorModalMessage = 'Please Register Organization before adding Category';
      this.errorModalBtnText = '';
      this.errorModalImgStatus = true;
      this.errorModalStatus = true;
    } else {
      this.httpService.addOrgGroupCategory(categoryname.viewModel).subscribe(response => {
        let apiResponse;
        apiResponse = response;
        apiResponse = apiResponse.response.result;
        if (apiResponse['status'] === 1) {
          this.categoryGroups.push({
            categoryGroupID: apiResponse.output.OrgCatGrpID.toString(),
            categoryGroupName: apiResponse.output.OrgCatGrpName,
            category: []
          });
          this.snackBarCreator('Category is added successfully');
          this.showCategoryFormStatus = false;
        } else {
          const errorCode = apiResponse['code']['Code'];
          const errorDesc = [];
          const errorMessage = apiResponse['description'];
          for (const value in errorMessage) {
            errorDesc.push(errorMessage[value]);
          }
          this.errorModalHeading = 'Error';
          this.errorModalMessage = errorDesc[0];
          this.errorModalBtnText = '';
          this.errorModalImgStatus = true;
          this.errorModalStatus = true;
        }
      });
    }
  }

  submitAddSUbCategory(categoryid, subcategoryname) {
    if (localStorage.getItem('E_OrgId') === null) {
      this.errorModalHeading = 'Error';
      this.errorModalMessage = 'Please Register Organization before adding Sub-Category';
      this.errorModalBtnText = '';
      this.errorModalImgStatus = true;
      this.errorModalStatus = true;
    } else {
      this.httpService.addOrgSubGroupCategory(categoryid, subcategoryname.viewModel).subscribe(response => {
        let apiResponse;
        apiResponse = response;
        apiResponse = apiResponse.response.result;
        if (apiResponse['status'] === 1) {
          const indexOfParentCategoryGroup = this.getIndexOfCategoryGroupByCategoryGroupId(categoryid, this.categoryGroups);
          this.categoryGroups[indexOfParentCategoryGroup].category.push({
            categoryId: apiResponse.output.OrgCatID.toString(),
            categoryName: apiResponse.output.OrgCatName
          });
          this.snackBarCreator('SubCategory is added successfully');
          this.showSubCategoryFormStatus = false;
        } else {
          const errorCode = apiResponse['code']['Code'];
          const errorDesc = [];
          const errorMessage = apiResponse['description'];
          for (const value in errorMessage) {
            errorDesc.push(errorMessage[value]);
          }
          this.errorModalHeading = 'Error';
          this.errorModalMessage = errorDesc[0];
          this.errorModalBtnText = '';
          this.errorModalImgStatus = true;
          this.errorModalStatus = true;
        }
      });
    }
  }
}
