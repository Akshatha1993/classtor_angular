import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-failure-modal',
  templateUrl: './failure-modal.component.html',
  styleUrls: ['./failure-modal.component.css']
})
export class FailureModalComponent implements OnInit {

  @Output() errorModalClose: EventEmitter<void> = new EventEmitter<void>();

  @Input() errorModalMessage: string;
  @Input() errorModalHeading: string;
  @Input() errorModalBtnText: string;
  @Input() errorModalImgStatus: boolean;
  constructor() { }

  ngOnInit() {
  }
  onCancelClick() {
    this.errorModalClose.emit();
  }
}
