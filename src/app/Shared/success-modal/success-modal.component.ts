import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-success-modal',
  templateUrl: './success-modal.component.html',
  styleUrls: ['./success-modal.component.css']
})
export class SuccessModalComponent implements OnInit {

  @Output() successModalClose: EventEmitter<void> = new EventEmitter<void>();
  @Output() onRegistration = new EventEmitter();
  @Input() successModalMessage: string;
  @Input() successModalHeading: string;
  @Input() successModalBtnText: string;
  @Input() successModalImgStatus: boolean;

  constructor() { }

  ngOnInit() {
  }

  onCancelClick() {
    this.successModalClose.emit();
  }

  onSuccessModalBtnClick(btnText) {
    this.onRegistration.emit();
  }
}
