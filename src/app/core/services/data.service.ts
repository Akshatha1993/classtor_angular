import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private refreshCommunication = new BehaviorSubject('');
  getrefreshCommunication = this.refreshCommunication.asObservable();

  private updateUserReadStatus = new BehaviorSubject('');
  getReadStatus = this.updateUserReadStatus.asObservable();

  private resObject = new BehaviorSubject('');
  getresponseObject = this.resObject.asObservable();

  private initiateCommunication = new BehaviorSubject('');
  getInitiatedCommunication = this.initiateCommunication.asObservable();

  private mapArray = new BehaviorSubject('');
  getMapArrayToMessage = this.mapArray.asObservable();

  private deleteMessage = new BehaviorSubject('');
  getDeteletMessage = this.deleteMessage.asObservable();

  private searchKeyWord = new BehaviorSubject('');
  getSearchKeyWord = this.searchKeyWord.asObservable();

  private undoMessage = new BehaviorSubject('');
  getUndoMessage = this.undoMessage.asObservable();

  private categoryMessages = new BehaviorSubject('');
  getCategoryMessages = this.categoryMessages.asObservable();

  private userEmailAddress = new BehaviorSubject('');
  getUserEmailAddress = this.userEmailAddress.asObservable();

  private createdClass = new BehaviorSubject('');
  getcreatedClass = this.createdClass.asObservable();

  private sentMesssage = new BehaviorSubject('');
  getsentMesssageStatus = this.sentMesssage.asObservable();

  constructor() { }

  setrefreshCommunication(obj: any) {
    this.refreshCommunication.next(obj);
  }

  setReadStatus(readStatusId: '') {
    this.updateUserReadStatus.next(readStatusId);
  }

  setMessageResObject(array: any) {
    this.resObject.next(array);
  }

  setInitiateCommunication(num: string) {
    this.initiateCommunication.next(num);
  }

  setMessages(array: any) {
    this.mapArray.next(array);
  }

  setDeteletMessage(obj: '') {
    this.deleteMessage.next(obj);
  }

  setSearchKeyWord(status: any) {
    this.searchKeyWord.next(status);
  }

  setUndoMessage(obj: '') {
    this.undoMessage.next(obj);
  }

  setCategoryMessages(obj: any) {
    this.categoryMessages.next(obj);
  }

  addUserEmailAddress(newAddress: string) {
    this.userEmailAddress.next(newAddress);
  }

  setCreatedClass(name: string) {
    this.createdClass.next(name);
  }

  setSentMessageStatus(status: any) {
    this.sentMesssage.next(status);
  }
}
