import {Component, Output, EventEmitter, Input} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {DataService} from '../../../../core/services/data.service';
import {HttpService} from '../../../../core/http/http.service';
import { PROJECT_ID } from '../../../../../environments/environment';

declare var $: any;
@Component({
  selector: 'app-feed-element',
  templateUrl: './feed-element.component.html',
  styleUrls: ['./feed-element.component.css']
})
export class FeedElementComponent {

  @Input() showReplyButton: boolean;
  @Input() postId: number;
  @Input() upvoted: boolean;
  @Input() upvoteCount: number;
  @Input() downvoteCount: number;
  @Input() replyCount: number;
  @Input() lastActive: string;
  @Input() feeddata: string;
  @Input() author: string;
  @Input() question: string;
  @Input() tag: string;
  @Input() profilePictureUrl: string;
  @Input() reported: boolean;
  @Input() undoStatus: boolean;
  @Input() reportPost: number;
  @Input() bgColor: string;
  @Output() enterIntoPostWithPostId = new EventEmitter();
  public userStatus = localStorage.getItem('E_UserClassType');

  constructor(private snackBar: MatSnackBar,
              private dataService: DataService,
              private httpService: HttpService) {
    this.upvoted = false;
    this.showReplyButton = true;
  }

  toggleUpvote(question) {
    // if (this.upvoted) {
    //   this.upvoted = false;
    //   this.upvoteCount--;
    // } else {
    //   this.upvoted = true;
    //   this.upvoteCount++;
    // }

    let countValue = '+1';
    let object;
    let backendObject;
    if (question.message.attribute.like.user.indexOf(localStorage.getItem('E_UserID')) > 0 || question.message.attribute.like.user.indexOf(localStorage.getItem('E_UserID')) === 0) {
      return;
    } else {

      if (question.message.attribute.dislike.user.indexOf(localStorage.getItem('E_UserID')) > 0 || question.message.attribute.dislike.user.indexOf(localStorage.getItem('E_UserID')) === 0) {
        object = [
          {
            last_updated: '',
            unique_id: '',
            read_status: 0,
            message: {
              unique_id: question.unique_id,
              receiver_id: question.message.receiver_id,
              message_type: 'ATTRIBUTE',
              attribute: {
                like: {
                  qty: '1',
                  user: [localStorage.getItem('E_UserID')],
                }
              },
              attributeType: 'like',
              sender_id: localStorage.getItem('E_UserID'),
              value: '+1'
            }
          },
        ];
        backendObject = [{
          unique_id: question.unique_id,
          sender_id: localStorage.getItem('E_UserID'),
          receiver_id: question.message.receiver_id,
          message_type: 'ATTRIBUTE',
          attribute: 'dislike',
          value: '-1',
          project_id: PROJECT_ID
        },
          {
            unique_id: question.unique_id,
            sender_id: localStorage.getItem('E_UserID'),
            receiver_id: question.message.receiver_id,
            message_type: 'ATTRIBUTE',
            attribute: 'like',
            value: '+1',
            project_id: PROJECT_ID
          }];

      } else {

        object = [{
          last_updated: '',
          unique_id: '',
          read_status: 0,
          message: {
            unique_id: question.unique_id,
            receiver_id: question.message.receiver_id,
            message_type: 'ATTRIBUTE',
            attribute: {
              like: {
                qty: '',
                user: [],
              }
            },
            attributeType: 'like',
            sender_id: '',
            value: countValue
          }
        }];

        object[0].message.sender_id = localStorage.getItem('E_UserID');
        object[0].message.attribute.like.qty = countValue;
        object[0].message.attribute.like.user[0] = localStorage.getItem('E_UserID');
        backendObject = [{
          unique_id: question.unique_id,
          sender_id: localStorage.getItem('E_UserID'),
          receiver_id: question.message.receiver_id,
          message_type: 'ATTRIBUTE',
          attribute: 'like',
          value: '+1',
          project_id: PROJECT_ID
        }];
      }
      this.dataService.setMessageResObject(object);
      this.httpService.addAttribute(backendObject).subscribe(response => {
      });
    }
  }

  toggleDownvote(question) {
    let countValue = '-1';
    let object;
    let backendObject;

    if (question.message.attribute.dislike.user.indexOf(localStorage.getItem('E_UserID')) > 0 || question.message.attribute.dislike.user.indexOf(localStorage.getItem('E_UserID')) === 0) {
      return;
    } else {

      if (question.message.attribute.like.user.indexOf(localStorage.getItem('E_UserID')) > 0 || question.message.attribute.like.user.indexOf(localStorage.getItem('E_UserID')) === 0) {
        question.message.attribute.dislike['childLikeButton'] = false;
        object = [
          {
            last_updated: '',
            unique_id: '',
            read_status: 0,
            message: {
              unique_id: question.unique_id,
              receiver_id: question.message.receiver_id,
              message_type: 'ATTRIBUTE',
              attribute: {
                dislike: {
                  qty: '1',
                  user: [localStorage.getItem('E_UserID')],
                }
              },
              attributeType: 'dislike',
              sender_id: localStorage.getItem('E_UserID'),
              value: '+1'
            }
          },
        ];
        backendObject = [{
          unique_id: question.unique_id,
          sender_id: localStorage.getItem('E_UserID'),
          receiver_id: question.message.receiver_id,
          message_type: 'ATTRIBUTE',
          attribute: 'dislike',
          value: '+1',
          project_id: PROJECT_ID
        },
          {
            unique_id: question.unique_id,
            sender_id: localStorage.getItem('E_UserID'),
            receiver_id: question.message.receiver_id,
            message_type: 'ATTRIBUTE',
            attribute: 'like',
            value: '-1',
            project_id: PROJECT_ID
          }];

      } else {
        object = [
          {
            last_updated: '',
            unique_id: '',
            read_status: 0,
            message: {
              unique_id: question.unique_id,
              receiver_id: question.message.receiver_id,
              message_type: 'ATTRIBUTE',
              attribute: {
                dislike: {
                  qty: '+1',
                  user: [localStorage.getItem('E_UserID')],
                }
              },
              attributeType: 'dislike',
              sender_id: localStorage.getItem('E_UserID'),
              value: '+1'
            }
          },
        ];


        backendObject = [{
          unique_id: question.unique_id,
          sender_id: localStorage.getItem('E_UserID'),
          receiver_id: question.message.receiver_id,
          message_type: 'ATTRIBUTE',
          attribute: 'dislike',
          value: '+1',
          project_id: PROJECT_ID
        }];
      }
      this.dataService.setMessageResObject(object);
      this.httpService.addAttribute(backendObject).subscribe(response => {
      });
    }
  }

  onPostEnter() {
    localStorage.setItem('mesUniqueId', this.postId.toString());
    this.enterIntoPostWithPostId.emit(this.postId);
  }

  reportToggle(question) {
    let countValue = '+1';
    let object;
    let backendObject;
    if (question.message.attribute.report.user.indexOf(localStorage.getItem('E_UserID')) > 0 || question.message.attribute.report.user.indexOf(localStorage.getItem('E_UserID')) === 0) {
      return;
    } else {

        object = [{
          last_updated: '',
          unique_id: '',
          read_status: 0,
          message: {
            unique_id: question.unique_id,
            receiver_id: question.message.receiver_id,
            message_type: 'ATTRIBUTE',
            attribute: {
              report: {
                qty: '',
                user: [],
              }
            },
            attributeType: 'report',
            sender_id: '',
            value: countValue
          }
        }];

        object[0].message.sender_id = localStorage.getItem('E_UserID');
        object[0].message.attribute.report.qty = countValue;
        object[0].message.attribute.report.user[0] = localStorage.getItem('E_UserID');
        backendObject = [{
          unique_id: question.unique_id,
          sender_id: localStorage.getItem('E_UserID'),
          receiver_id: question.message.receiver_id,
          message_type: 'ATTRIBUTE',
          attribute: 'report',
          value: '+1',
          project_id: PROJECT_ID
        }];
      }
      this.dataService.setMessageResObject(object);
      this.httpService.addAttribute(backendObject).subscribe(response => {
      });
  }

  // utility functions
  snackBarCreator(message: string) {
    this.snackBar.open(message, 'Close', {
      duration: 1000,
    });
  }

  hidePost(question) {
    // this.snackBarCreator('Post Has been hidden');
    if (localStorage.getItem('E_UserStatus') !== '0') {
      this.httpService.deleteMessages(question.unique_id, '0').subscribe(response => {
        let apiResponse;
        apiResponse = response;
        if (apiResponse['status'] === 'success') {
          this.dataService.setDeteletMessage(question);
        }
      });
    } else {
      $.toast({
        heading: 'Success',
        text: 'You do not have permission to hide the post',
        bgColor: '#FF7D54',
        loaderBg: '#f7f7f7',              // Background color for toast
        textColor: '#d9d9d9',            // text color
        allowToastClose: true,       // Show the close button or not
        hideAfter: 4000,              // `false` to make it sticky or time in miliseconds to hide after
        position: 'top-right',
        icon: 'success'
      });
    }
    }

  undoPost(question) {
    this.httpService.deleteMessages(question.unique_id, '1').subscribe(response => {
      let apiResponse;
      apiResponse = response;
      if (apiResponse['status'] === 'success') {
        this.dataService.setUndoMessage(question);
      }
    });
  }
}
