import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Form, FormControl, FormGroup, Validators} from "@angular/forms";
import {DataService} from "../../../core/services/data.service";
import {HttpService} from "../../../core/http/http.service";

declare var $: any;
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  forgotPasswordForm2: FormGroup;
  resetPasswordStatus = false;
  forgotPasswordErrorStatus = false;
  forgotPasswordEmailErrorStatus = false;
  @Output() loginScreen = new EventEmitter();

  email;
  showLogo = false;

  // For the Error Modal.
  errorModalMessage;
  errorModalHeading;
  errorModalBtnText;
  errorModalImgStatus;
  errorModalStatus = false;
  constructor(private dataService: DataService,
              private httpService: HttpService) { }

  ngOnInit() {
    this.forgotPasswordForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]),
    });

    this.forgotPasswordForm2 = new FormGroup({
      passcode: new FormControl('', [Validators.required, Validators.pattern(/^.{4,4}$/)]),
      password: new FormControl('', [Validators.required, Validators.pattern(/^.{6,25}$/)]),
      confirmpassword: new FormControl('', [Validators.required, Validators.pattern(/^.{6,25}$/)]),
    });

    const queryString = window.location.search.substring(1);
    if (queryString !== '') {
      const urlParams = queryString.split('&');
      const InstituteID = urlParams[0].split('=');
      if (InstituteID[1] !== 'e9Z5w8') {
        this.showLogo = true;
      }
      // this.email = ID[1];
    }
  }

  resetPassword() {
    if (this.forgotPasswordForm.value.email === '') {
      this.resetPasswordStatus = false;
      this.forgotPasswordEmailErrorStatus = false;
    } else {
        this.httpService.forgotPassword(this.forgotPasswordForm.value.email,
        ).subscribe(response => {
          let apiResponse = response;
          apiResponse = apiResponse['response']['result'];
          if (apiResponse['status'] !== 0) {

            if (apiResponse['status'] !== 0) {
              $.toast({
                heading: 'Success',
                text: 'Passcode has been sent your email successfully',
                bgColor: '#b3cde0',
                loaderBg: '#005b96',                // Background color for toast
                textColor: '#000',            // text color
                allowToastClose: true,       // Show the close button or not
                hideAfter: 4000,              // `false` to make it sticky or time in miliseconds to hide after
                position: 'top-right',
                icon: 'success'
              });
              this.resetPasswordStatus = true;
            }
          } else {
            const errorCode = apiResponse['code']['Code'];
            const errorDesc = [];
            const errorMessage = apiResponse['description'];
            for (const value in errorMessage) {
              errorDesc.push(errorMessage[value]);
            }
            this.errorModalHeading = 'Error';
            this.errorModalMessage = errorDesc[0];
            this.errorModalBtnText = '';
            this.errorModalImgStatus = true;
            this.errorModalStatus = true;
          }

        });
    }

  }
  toLoginScreen(){
    this.loginScreen.emit();
  }
  onforgotPassword() {
    if (this.forgotPasswordForm2.value.passcode === '' || this.forgotPasswordForm2.value.password === '' || this.forgotPasswordForm2.value.confirmpassword === '') {
      this.forgotPasswordErrorStatus = true;
    } else {
      if (this.forgotPasswordForm2.value.password === this.forgotPasswordForm2.value.confirmpassword) {
        this.httpService.userResetPassword(this.forgotPasswordForm.value.email,
          this.forgotPasswordForm2.value.passcode, this.forgotPasswordForm2.value.password).subscribe(response => {
          this.dataService.addUserEmailAddress(this.forgotPasswordForm.value.email);
          let apiResponse = response;
          apiResponse = apiResponse['response']['result'];
          if (apiResponse['status'] !== 0) {

            if (apiResponse['status'] !== 0) {
              $.toast({
                heading: 'Success',
                text: 'Your password has been resetted successfully',
                bgColor: '#1da1f2',
                loaderBg: '#f7f7f7',              // Background color for toast
                textColor: '#d9d9d9',            // text color
                allowToastClose: true,       // Show the close button or not
                hideAfter: 4000,              // `false` to make it sticky or time in miliseconds to hide after
                position: 'top-right',
                icon: 'success'
              });
              this.loginScreen.emit();
            }
          } else {
            const errorCode = apiResponse['code']['Code'];
            const errorDesc = [];
            const errorMessage = apiResponse['description'];
            for (const value in errorMessage) {
              errorDesc.push(errorMessage[value]);
            }
            this.errorModalHeading = 'Error';
            this.errorModalMessage = errorDesc[0];
            this.errorModalBtnText = '';
            this.errorModalImgStatus = true;
            this.errorModalStatus = true;
          }

        });
      }
    }
  }


  closeErrorModal() {
    this.errorModalStatus = false;
  }
}
