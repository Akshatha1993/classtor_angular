import {Component, Input, OnInit} from '@angular/core';
import {DiscussionComment} from '../../../../../Shared/interfaces';
import {DataService} from "../../../../../core/services/data.service";
import {HttpService} from "../../../../../core/http/http.service";

@Component({
  selector: 'app-discussion-feed',
  templateUrl: './discussion-feed.component.html',
  styleUrls: ['./discussion-feed.component.css']
})
export class DiscussionFeedComponent implements OnInit {

  @Input() answerId: string;
  commentFeedOfAnswer: DiscussionComment[];
  comments;
  askQuestionMapArray;

  constructor(private dataService: DataService,
              private httpService: HttpService) {
    this.dataService.getMapArrayToMessage.subscribe(response => {
      let convertedDateTime;
      let time;
      this.askQuestionMapArray = response;
      if (this.askQuestionMapArray !== '') {
        if (this.askQuestionMapArray.get(localStorage.getItem('E_classId')).get(localStorage.getItem('commentUniqueId')) !== undefined) {
          this.comments = this.askQuestionMapArray.get(localStorage.getItem('E_classId')).get(localStorage.getItem('commentUniqueId'));
          if (this.comments.length > 0) {
            for (let j = 0; j < this.comments.length; j++) {
              time = this.comments[j].message.time.slice(0, -3);
              convertedDateTime = this.time2TimeAgo(j, Number(time));
              this.comments[j].message['timezone'] = convertedDateTime;
            }
          }
        }
      }
    });
  }

  ngOnInit() {
    this.commentFeedOfAnswer = this.getDiscussionsByAnswerId();
  }

  time2TimeAgo(i, time) {
    // This function computes the delta between the
    // provided timestamp and the current time, then test
    // the delta for predefined ranges.

    const d = new Date();  // Gets the current time
    const nowTs = Math.floor(d.getTime() / 1000); // getTime() returns milliseconds, and we need seconds, hence the Math.floor and division by 1000
    const seconds = nowTs - time;

    // morethan one year.
    if (seconds > 3.154e+7) {
      return Math.floor(seconds / 3.154e+7) + ' years ago';
    }

    // more that two days
    if (seconds > 2 * 24 * 3600) {
      return Math.floor(seconds / 86400) + ' days ago';
    }
    // a day
    // if (seconds > 24 * 3600) {
    //   console.log(Math.floor(seconds / 86400) + 'yesterday');
    //   return Math.floor(seconds / 86400) + ' yesterday';
    // }

    if (seconds > 3600) {
      return Math.floor(seconds / 3600) + ' hour ago';
    }
    // if (seconds > 1800) {
    //   console.log('Half an hour ago');
    // }
    if (seconds > 60) {
      return Math.floor(seconds / 60) + ' minute ago';
    }

    if (seconds < 60) {
      // return Math.floor(seconds) + ' seconds ago';
      return 'Just now';
    }
  }
  getDiscussionsByAnswerId() {

    // TODO: API CALL HERE ...

    const discussionFeedOfAnswer: DiscussionComment[] = [
      {
        authorDisplayName: 'Anonymous',
        comment: 'This is a sample comment.',
        dateTimePosted: 'A few days ago.'
      },
      {
        authorDisplayName: 'Stan Lee',
        comment: 'Another Comment.',
        dateTimePosted: 'A few years ago.'
      },
      {
        authorDisplayName: 'Captain Marvel',
        comment: 'The first comment.',
        dateTimePosted: 'A few lightyears ago.'
      }
    ];
    return discussionFeedOfAnswer;
  }

}
