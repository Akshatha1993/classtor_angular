import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../core/services/data.service';
import {HttpService} from '../../../core/http/http.service';

declare var $: any;
@Component({
  selector: 'app-confirm-passcode',
  templateUrl: './confirm-passcode.component.html',
  styleUrls: ['./confirm-passcode.component.css']
})
export class ConfirmPasscodeComponent implements OnInit {
  confirmPasscodeForm: FormGroup;
  confirmPasscodeErrorStatus = false;
  email;
  showLogo = false;

  // For the Error Modal.
  errorModalMessage;
  errorModalHeading;
  errorModalBtnText;
  errorModalImgStatus;
  errorModalStatus = false;

  @Output() loginScreen = new EventEmitter();

  constructor(private dataService: DataService,
              private httpService: HttpService) {

  }

  ngOnInit() {
    this.confirmPasscodeForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]),
      passcode: new FormControl('', [Validators.required, Validators.pattern(/^.{4,4}$/)]),
    });

    this.dataService.getUserEmailAddress.subscribe(emailAddress =>
      this.email = emailAddress
    );

    const queryString = window.location.search.substring(1);
    if (queryString !== '') {
      const urlParams = queryString.split('&');
      const InstituteID = urlParams[0].split('=');
      if (InstituteID[1] !== 'e9Z5w8') {
        this.showLogo = true;
      }
      // this.email = ID[1];
    }
  }

  onConfirmPasscode() {
    if (this.confirmPasscodeForm.value.email === '' || this.confirmPasscodeForm.value.passcode === '') {
      this.confirmPasscodeErrorStatus = true;
    } else {
      if (this.confirmPasscodeForm.valid === true) {
        this.httpService.confirmPasscode(this.confirmPasscodeForm.value.email,
          this.confirmPasscodeForm.value.passcode).subscribe(response => {
          this.dataService.addUserEmailAddress(this.confirmPasscodeForm.value.email);
          let apiResponse = response;
          apiResponse = apiResponse['response']['result'];
          if (apiResponse['status'] !== 0) {

            if (apiResponse['status'] !== 0) {
              $.toast({
                heading: 'Success',
                text: 'Passcode verification is successful',
                bgColor: '#b3cde0',
                loaderBg: '#005b96',                // Background color for toast
                textColor: '#000',            // text color
                allowToastClose: true,       // Show the close button or not
                hideAfter: 4000,              // `false` to make it sticky or time in miliseconds to hide after
                position: 'top-right',
                icon: 'success'
              });
              this.loginScreen.emit();
            }
          } else {
            const errorCode = apiResponse['code']['Code'];
            const errorDesc = [];
            const errorMessage = apiResponse['description'];
            for (const value in errorMessage) {
              errorDesc.push(errorMessage[value]);
            }
            this.errorModalHeading = 'Error';
            this.errorModalMessage = errorDesc[0];
            this.errorModalBtnText = '';
            this.errorModalImgStatus = true;
            this.errorModalStatus = true;
          }

        });
      }
    }
  }

  onResendPasscode() {
    if (this.confirmPasscodeForm.value.email === '') {
      this.confirmPasscodeErrorStatus = true;
    } else {
      if (this.confirmPasscodeForm.valid === true) {
        this.httpService.resendPasscode(this.confirmPasscodeForm.value.email,
          ).subscribe(response => {
          let apiResponse = response;
          apiResponse = apiResponse['response']['result'];
          if (apiResponse['status'] !== 0) {

            if (apiResponse['status'] !== 0) {
              $.toast({
                heading: 'Success',
                text: 'Passcode has been sent to your registered mail',
                bgColor: '#1da1f2',
                loaderBg: '#f7f7f7',              // Background color for toast
                textColor: '#d9d9d9',            // text color
                allowToastClose: true,       // Show the close button or not
                hideAfter: 4000,              // `false` to make it sticky or time in miliseconds to hide after
                position: 'top-right',
                icon: 'success'
              });
            }
          } else {
            const errorCode = apiResponse['code']['Code'];
            const errorDesc = [];
            const errorMessage = apiResponse['description'];
            for (const value in errorMessage) {
              errorDesc.push(errorMessage[value]);
            }
            this.errorModalHeading = 'Error';
            this.errorModalMessage = errorDesc[0];
            this.errorModalBtnText = '';
            this.errorModalImgStatus = true;
            this.errorModalStatus = true;
          }

        });
      }
    }
  }

  closeErrorModal() {
    this.errorModalStatus = false;
  }
}
