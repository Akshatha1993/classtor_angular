import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class ChosenUsersService {
  private chosenUserIds = new BehaviorSubject<string[]>([]);
  currentList = this.chosenUserIds.asObservable();

  updateChosenUserList(updatedChosenUserList: string[]) {
    this.chosenUserIds.next(updatedChosenUserList);
  }

  addToChosenUserList(UserId: string, status) {
    let updated = this.chosenUserIds.getValue();
    if (updated.length === 0) {
      let object;
      object = {
        userId: UserId,
        userStatus: status
      };
      updated.push(object);
    } else {
      for (let i = 0; i < updated.length; i++) {
        const indexToRemove = updated.findIndex(x => x['userId'] === UserId);
        if (indexToRemove !== -1) {
          updated[indexToRemove]['userStatus'] = status;
        } else {
          let object;
          object = {
            userId: UserId,
            userStatus: status
          };
          updated.push(object);
        }
      }
    }
    this.updateChosenUserList(updated);
  }

  removeFromChosenUserList(UserId: string, status) {
    let updated = this.chosenUserIds.getValue();
    for (let i = 0; i < updated.length; i++) {
      const indexToRemove = updated.findIndex(x => x['userId'] === UserId);
      if (indexToRemove >= 0) {
        updated[indexToRemove]['userStatus'] = status;
      } else {
        let object;
        object = {
          userId: UserId,
          userStatus: status
        };
        updated.push(object);
      }
    }
    this.updateChosenUserList(updated);
  }

  clearSlate() {
    this.updateChosenUserList([]);
  }
}
