import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {HttpService} from '../../../core/http/http.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../core/services/data.service';

declare var $: any;

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  isRegistration = false;
  @Output() loginScreen = new EventEmitter();
  @Output() onSuccessfulRegistration = new EventEmitter();
  registrationForm: FormGroup;
  registrationErrorStatus = false;
  showLogo = false;

  // For the Success Modal.
  @Output() successModalMessage = new EventEmitter();
  @Output() successModalHeading = new EventEmitter();
  @Output() successModalBtnText = new EventEmitter();
  @Output() successModalImgStatus = new EventEmitter();
  @Output() successModalStatus = new EventEmitter();

  // For the Error Modal.
  errorModalMessage;
  errorModalHeading;
  errorModalBtnText;
  errorModalImgStatus;
  errorModalStatus = false;

  constructor(private httpService: HttpService,
              private dataService: DataService) { }

  ngOnInit() {
    this.registrationForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]),
      username: new FormControl('', [Validators.required, Validators.pattern(/^.{1,24}$/)]),
      password: new FormControl('', [Validators.required, Validators.pattern(/^.{6,15}$/)]),
    });

    const queryString = window.location.search.substring(1);
    if (queryString !== '') {
      const urlParams = queryString.split('&');
      const InstituteID = urlParams[0].split('=');
      if (InstituteID[1] !== '') {
        this.showLogo = true;
      }
      // this.email = ID[1];
    }
  }

  toLoginScreen() {
    this.loginScreen.emit();
  }

  onRegistration() {
    if (this.registrationForm.value.email === '' || this.registrationForm.value.username === '' || this.registrationForm.value.password === '' ) {
      this.registrationErrorStatus = true;
    } else {
      if (this.registrationForm.valid === true) {
        this.httpService.userRegistration(this.registrationForm.value.username, this.registrationForm.value.email,
          this.registrationForm.value.password).subscribe(response => {
          this.dataService.addUserEmailAddress(this.registrationForm.value.email);
          // this.onSuccessfulRegistration.emit();
          let apiResponse = response;
          apiResponse = apiResponse['response']['result'];
          if (apiResponse['status'] !== 0) {
            // this.successModalHeading.emit('Registration Success');
            // this.successModalMessage.emit('Registration request is successful and OTP is sent to your registered email.');
            // this.successModalBtnText.emit('Continue');
            // this.successModalImgStatus.emit(false);
            //
            // this.successModalStatus.emit(true);
            if (apiResponse['status'] !== 0) {
              $.toast({
                heading: 'Success',
                text: 'Registration request is successful and OTP is sent to your registered email.',
                bgColor: '#b3cde0',
                loaderBg: '#005b96',              // Background color for toast
                textColor: '#000',            // text color
                allowToastClose: true,       // Show the close button or not
                hideAfter: 4000,              // `false` to make it sticky or time in miliseconds to hide after
                position: 'top-right',
                icon: 'success'
              });
              this.onSuccessfulRegistration.emit();
            }
          } else {
            const errorCode = apiResponse['code']['Code'];
            const errorDesc = [];
            const errorMessage = apiResponse['description'];
            for (const value in errorMessage) {
              errorDesc.push(errorMessage[value]);
            }
            this.errorModalHeading = 'Error';
            this.errorModalMessage = errorDesc[0];
            this.errorModalBtnText = '';
            this.errorModalImgStatus = true;
            this.errorModalStatus = true;
          }

        });
      }
    }
  }

  closeErrorModal() {
    this.errorModalStatus = false;
  }


  // onSuccessfulRegister() {
  //
  // }
}
