import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {ObjectMapper} from 'json-object-mapper';
import {catchError} from 'rxjs/operators';
import { HttpService } from '../core/http/http.service';
import { HttpClient,HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-profile-screen',
  templateUrl: './profile-screen.component.html',
  styleUrls: ['./profile-screen.component.css']
})
export class ProfileScreenComponent implements OnInit {
@Output() changePassScreen=new EventEmitter();

public classList;
public userDetails;

  constructor(private http:HttpService,private _http:HttpClient) { 
    this.http.getClassList().subscribe((response: any)=>{
      this.classList=response.response.result;
      
    });
    this.http.getUserDetails().subscribe((response: any)=>{
      this.userDetails=response.response.result;
      
      
    });
  }
  toChangePassword() {
    this.changePassScreen.emit();
}
intialPic="assets/icon/profile.jpeg";
newPic:File=null;

changePic(file:FileList){
  
  this.newPic=file.item(0);
  /*const fd=new FormData();
  fd.append('image',this.newPic);
  this._http.post("app.component.html",fd,{reportProgress:true,observe:'events'}).
  subscribe(event =>{
    if(event.type==HttpEventType.UploadProgress){
      console.log(`Uploading ${Math.round( event.loaded/event.total*100)}%`)
    }
    console.log(event )});*/
  var reader =new FileReader();
  reader.onload=(event:any)=>{
    this.intialPic=event.target.result;
  }
  reader.readAsDataURL(this.newPic);
}

  ngOnInit() {
    
  }

}
