import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {ChangePasswordComponent} from './pages/authentication/change-password/change-password.component';
import {ProfileScreenComponent} from './profile-screen/profile-screen.component';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatButtonModule, MatButtonToggleModule,
  MatCardModule, MatChipsModule,
  MatIconModule,
  MatListModule, MatMenuModule,
  MatRadioModule,
  MatRippleModule, MatSidenavModule, MatTabsModule, MatToolbarModule, MatTooltipModule,
  MatProgressSpinnerModule
} from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {SignInComponent} from './pages/authentication/sign-in/sign-in.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { SidebarComponent } from './pages/sidebar/sidebar.component';
import { MainComponent } from './pages/main/main/main.component';
import { SearchAndFiltersComponent } from './pages/main/search-and-filters/search-and-filters.component';
import { FeedComponent } from './pages/main/feed/feed/feed.component';
import { FeedElementComponent } from './pages/main/feed/feed-element/feed-element.component';
import { PostQuestionComponent } from './pages/main/post-question/post-question.component';
import { NgxEditorModule } from 'ngx-editor';
import { HttpClientModule } from '@angular/common/http';
import {HTTPListener, HttpStatusService} from './core/http/HTTPListener';
import {HttpService} from './core/http/http.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import { CommonModule } from '@angular/common';
// tslint:disable-next-line:max-line-length
import { SingleQuestionWithAnswersComponent } from './pages/main/single-question-with-answers/single-question-with-answers.component';
import { AnswerElementComponent } from './pages/main/answer-feed/answer-element/answer-element.component';
import { AnswerFeedComponent } from './pages/main/answer-feed/answer-feed/answer-feed.component';
import { ShinyTextComponent } from './utility/shiny-text/shiny-text.component';
import { DiscussionFeedComponent } from './pages/main/answer-feed/discussion-feed/discussion-feed/discussion-feed.component';
// tslint:disable-next-line:max-line-length
import { DiscussionElementComponent } from './pages/main/answer-feed/discussion-feed/discussion-element/discussion-element.component';
import { CreateClassroomComponent } from './pages/main/instructor/create-classroom/create-classroom.component';
// tslint:disable-next-line:max-line-length
import { ManageClassroomStudentsComponent } from './pages/main/instructor/manage/manage-classroom-students/manage-classroom-students.component';

import {FlexLayoutModule} from '@angular/flex-layout';
// tslint:disable-next-line:max-line-length
// import { ManageClassroomInstructorsComponent } from './pages/main/instructor/manage/manage-classroom-instructors/manage-classroom-instructors.component';
import {ChosenUsersService} from './Shared/chosen-users.service';
// tslint:disable-next-line:max-line-length
import {SelectUserElementComponent} from './pages/main/instructor/manage/select-user-element/select-user-element.component';
// import { ResetPasswordComponent } from './pages/authentication/reset-password/reset-password.component';
// import { ResponsiveDesignComponent } from './_playground/responsive-design/responsive-design.component';
import {HttpModule} from '@angular/http';
import {NgxSpinnerModule} from 'ngx-spinner';
import { RegistrationComponent } from './pages/authentication/registration/registration.component';
import { ConfirmPasscodeComponent } from './pages/authentication/confirm-passcode/confirm-passcode.component';
import { ForgotPasswordComponent } from './pages/authentication/forgot-password/forgot-password.component';
import { SuccessModalComponent } from './Shared/success-modal/success-modal.component';
import { FailureModalComponent } from './Shared/failure-modal/failure-modal.component';
import { TeacherFeedComponent } from './pages/main/answer-feed/teacher-feed/teacher-feed/teacher-feed.component';
import { TeacherElementComponent } from './pages/main/answer-feed/teacher-feed/teacher-element/teacher-element.component';
import { ManageClassroomInstructorsComponent } from './pages/main/instructor/manage/manage-classroom-instructors/manage-classroom-instructors/manage-classroom-instructors.component';
import { OrganizationRegisterComponent } from './pages/authentication/organization-register/organization-register.component';
import { VerifyPasscodeComponent } from './pages/authentication/verify-passcode/verify-passcode.component';
import { Routes, RouterModule } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    // ResetPasswordComponent,
    SidebarComponent,
    MainComponent,
    SearchAndFiltersComponent,
    FeedComponent,
    FeedElementComponent,
    PostQuestionComponent,
    SingleQuestionWithAnswersComponent,
    AnswerElementComponent,
    AnswerFeedComponent,
    ShinyTextComponent,
    DiscussionFeedComponent,
    DiscussionElementComponent,
    CreateClassroomComponent,
    ManageClassroomStudentsComponent,
    SelectUserElementComponent,
    RegistrationComponent,
    ConfirmPasscodeComponent,
    ForgotPasswordComponent,
    SuccessModalComponent,
    FailureModalComponent,
    TeacherFeedComponent,
    TeacherElementComponent,
    ManageClassroomInstructorsComponent,
    OrganizationRegisterComponent,
    ProfileScreenComponent,
    ChangePasswordComponent,
    VerifyPasscodeComponent,
    // ManageClassroomInstructorsComponent,
    // ResponsiveDesignComponent,
  ],
  imports: [
    HttpModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    FormsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    RouterModule.forRoot([]),
    DragDropModule,
    MatListModule,
    MatIconModule,
    MatBadgeModule,
    MatRippleModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatToolbarModule,
    NgxEditorModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatMenuModule,
    FlexLayoutModule,
    NgxSpinnerModule,
    MatTabsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [HttpService, HttpStatusService, {
    provide: HTTP_INTERCEPTORS,
    useClass: HTTPListener,
    multi: true
  }, ChosenUsersService],
  bootstrap: [AppComponent]
})
export class AppModule {

}
