import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {Select} from '../../post-question/post-question.component';
import {DataService} from '../../../../core/services/data.service';
import {HttpService} from '../../../../core/http/http.service';
import * as sha1 from 'sha1/sha1';
import { PROJECT_ID } from '../../../../../environments/environment';

@Component({
  selector: 'app-answer-element',
  templateUrl: './answer-element.component.html',
  styleUrls: ['./answer-element.component.css']
})
export class AnswerElementComponent {

  @Input() parentPostId: string;
  @Input() upvoted: boolean;
  @Input() upvoteCount: number;
  @Input() discussionCount: number;
  @Input() lastEdited: string;
  @Input() author: string;
  @Input() answer: string;
  @Input() authorDisplayImage: string;
  @Input() trophyCount: number;
  @Input() reported: boolean;
  @Input() answerData: string;
  @Input() reportPost: number;

  public showDiscussions = false;
  public showCommentBox: boolean;
  public replying = false;
  public replyValue = 'Reply';
  public userStatus = localStorage.getItem('E_UserStatus');

  public commentText = '';

  selectedAnonymityScopeId = 1;
  defaultAnonymityScope = '1';
  anonymityScope: Select[] = [
    {id: '0', userid: localStorage.getItem('E_UserID'), viewValue: 'Anonymous'},
    {id: '1', userid: localStorage.getItem('E_UserID'), viewValue: localStorage.getItem('E_UserName')},
  ];

  constructor(public snackBar: MatSnackBar,
              public dataService: DataService,
              public httpService: HttpService) {
    this.author = 'Anonymous';
    this.answer = 'The answer to this question is so absurdly simple that I am going' +
                  ' to write it in a pretty long sentence on how absurd it is that ' +
                  'I am trying to dodge the question to show it\'s absurdness.';
    this.upvoteCount = 4;
    this.discussionCount = 8;
    this.lastEdited = '4 hours ago';
    this.upvoted = false;
    this.showDiscussions = false;
  }

  toggleUpvote(answer) {
    let countValue = '+1';
    let object;
    let backendObject;
    if (answer.message.attribute.like.user.indexOf(localStorage.getItem('E_UserID')) > 0 || answer.message.attribute.like.user.indexOf(localStorage.getItem('E_UserID')) === 0) {
      return;
    } else {

      if (answer.message.attribute.dislike.user.indexOf(localStorage.getItem('E_UserID')) > 0 || answer.message.attribute.dislike.user.indexOf(localStorage.getItem('E_UserID')) === 0) {
        object = [
          {
            last_updated: '',
            unique_id: '',
            read_status: 0,
            message: {
              unique_id: answer.unique_id,
              receiver_id: answer.message.receiver_id,
              message_type: 'ATTRIBUTE',
              attribute: {
                like: {
                  qty: '1',
                  user: [localStorage.getItem('E_UserID')],
                }
              },
              attributeType: 'like',
              sender_id: localStorage.getItem('E_UserID'),
              value: '+1'
            }
          },
        ];
        backendObject = [{
          unique_id: answer.unique_id,
          sender_id: localStorage.getItem('E_UserID'),
          receiver_id: answer.message.receiver_id,
          message_type: 'ATTRIBUTE',
          attribute: 'dislike',
          value: '-1',
          project_id: PROJECT_ID
        },
          {
            unique_id: answer.unique_id,
            sender_id: localStorage.getItem('E_UserID'),
            receiver_id: answer.message.receiver_id,
            message_type: 'ATTRIBUTE',
            attribute: 'like',
            value: '+1',
            project_id: PROJECT_ID
          }];

      } else {

        object = [{
          last_updated: '',
          unique_id: '',
          read_status: 0,
          message: {
            unique_id: answer.unique_id,
            receiver_id: answer.message.receiver_id,
            message_type: 'ATTRIBUTE',
            attribute: {
              like: {
                qty: '',
                user: [],
              }
            },
            attributeType: 'like',
            sender_id: '',
            value: countValue
          }
        }];

        object[0].message.sender_id = localStorage.getItem('E_UserID');
        object[0].message.attribute.like.qty = countValue;
        object[0].message.attribute.like.user[0] = localStorage.getItem('E_UserID');
        backendObject = [{
          unique_id: answer.unique_id,
          sender_id: localStorage.getItem('E_UserID'),
          receiver_id: answer.message.receiver_id,
          message_type: 'ATTRIBUTE',
          attribute: 'like',
          value: '+1',
          project_id: PROJECT_ID
        }];
      }
      this.dataService.setMessageResObject(object);
      this.httpService.addAttribute(backendObject).subscribe(response => {
      });
    }
  }

  showAnswerDiscussions(answer) {
    localStorage.setItem('commentUniqueId', answer.unique_id);
    this.showDiscussions = !this.showDiscussions;
  }

  toggleReply(answer) {
    localStorage.setItem('commentUniqueId', answer.unique_id);
    this.showDiscussions = true;
    this.replying = !this.replying;
    this.showCommentBox = !this.showCommentBox;
    if (!this.replying) {
      this.replyValue = 'Reply';
    } else {
      this.replyValue = 'Cancel Reply';
    }
  }

  postDiscussionComment() {
    this.replying = false;
    this.showCommentBox = false;
    this.replyValue = 'Reply';
    this.commentText = '';
    this.openSnackBar('Comment Posted');
  }

  getAnonymityScopeNameFromChosenAnonymityScopeId() {
    for (const anonymityScope of this.anonymityScope) {
      if (anonymityScope.id === this.selectedAnonymityScopeId.toString()) {
        return anonymityScope.viewValue;
      }
    }
  }

  onSelectionChangedAnonymityLevel($event) {
    this.selectedAnonymityScopeId = $event.value;
    this.openSnackBar('You will be posting as ' + this.getAnonymityScopeNameFromChosenAnonymityScopeId());
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Close', {
      duration: 2000,
    });
  }

  reportToggle(answer) {
    let countValue = '+1';
    let object;
    let backendObject;
    if (answer.message.attribute.report.user.indexOf(localStorage.getItem('E_UserID')) > 0 || answer.message.attribute.report.user.indexOf(localStorage.getItem('E_UserID')) === 0) {
      return;
    } else {

      object = [{
        last_updated: '',
        unique_id: '',
        read_status: 0,
        message: {
          unique_id: answer.unique_id,
          receiver_id: answer.message.receiver_id,
          message_type: 'ATTRIBUTE',
          attribute: {
            report: {
              qty: '',
              user: [],
            }
          },
          attributeType: 'report',
          sender_id: '',
          value: countValue
        }
      }];

      object[0].message.sender_id = localStorage.getItem('E_UserID');
      object[0].message.attribute.report.qty = countValue;
      object[0].message.attribute.report.user[0] = localStorage.getItem('E_UserID');
      backendObject = [{
        unique_id: answer.unique_id,
        sender_id: localStorage.getItem('E_UserID'),
        receiver_id: answer.message.receiver_id,
        message_type: 'ATTRIBUTE',
        attribute: 'report',
        value: '+1',
        project_id: PROJECT_ID
      }];
    }
    this.dataService.setMessageResObject(object);
    this.httpService.addAttribute(backendObject).subscribe(response => {
    });
  }

  postAnswer(question) {


    let messageParent;
    messageParent = localStorage.getItem('commentUniqueId');
    let localCategory;
    localCategory = question.message.category;
    let responseObject;
    responseObject = [{
      unique_id: '',
      message: {
        parent: '',
        sender_points: '',
        unique_id: '',
        project_id: PROJECT_ID,
        receiver_id: question.message.receiver_id,
        receiver_name: question.message.receiver_name,
        sender_name: this.getAnonymityScopeNameFromChosenAnonymityScopeId(),
        sender_id: localStorage.getItem('E_UserID'),
        message_type: 'TEXT',
        message: {
          msg: '',
        },
        receiver_type: 'I',
        time: '',
        category: localCategory,
        attribute: {
          like: {
            qty: '0',
            user: [],
          },
          dislike: {
            qty: '0',
            user: [],
          },
          comment: {
            qty: '0',
            user: [],
          }
        }

      },
      last_updated: ''
    }];
    let randomNum;
    let encryptedPassword;
    let stringTime;
    let hashedNum;
    let h;
    let unixTime;
    let timstamp;
    let object;
    let messagetext;
    messagetext = this.commentText;
    randomNum = Math.floor(Math.random() * 90000) + 10000;
    encryptedPassword = sha1(localStorage.getItem('E_UserID') + messagetext + randomNum);
    hashedNum = encryptedPassword.substring(0, 15);

    let result;
    let messageJson;
    responseObject[0].unique_id = (hashedNum);
    responseObject[0].message.message.msg = messagetext;
    responseObject[0].message.parent = messageParent;
    responseObject[0].message.unique_id = hashedNum;
    responseObject[0].message.sender_points = null
    unixTime = new Date();
    timstamp = unixTime.getTime();
    h = timstamp;
    stringTime = h.toString();

    responseObject[0].message.time = stringTime;
    responseObject[0].last_updated = stringTime;
    messageJson = {
      msg: messagetext,
    };

    object = [
      {
        unique_id: hashedNum,
        receiver_type: 'I',
        sender_name: this.getAnonymityScopeNameFromChosenAnonymityScopeId(),
        sender_id: localStorage.getItem('E_UserID'),
        receiver_name: question.message.receiver_name,
        receiver_id: question.message.receiver_id,
        sender_points: null,
        message_type: 'TEXT',
        message: messageJson,
        parent: messageParent,
        project_id: PROJECT_ID,
        random_key: randomNum,
        category: localCategory,
        attribute: {
          like: {
            qty: '0',
            user: [],
          },
          dislike: {
            qty: '0',
            user: [],
          },
          comment: {
            qty: '0',
            user: [],
          }
        }
      }
    ];
    this.dataService.setMessageResObject(responseObject);
    this.sendCommentAttribute(messageParent, question.message.receiver_id);
    this.commentText = '';
    // this.showAnswerBar = !this.showAnswerBar;
    this.httpService.sendMessage(object).subscribe(response => {
      result = response;
      if (result.status === 'success') {
        this.commentText = '';
      }
    });
  }

  sendCommentAttribute(parentuniqueid, recieverid) {
    let object;
    let backendObject;
    object = [{
      last_updated: '',
      unique_id: '',
      read_status: 0,
      message: {
        unique_id: parentuniqueid,
        receiver_id: recieverid,
        message_type: 'ATTRIBUTE',
        attribute: {
          comment: {
            qty: '',
            user: [],
          }
        },
        attributeType: 'comment',
        sender_id: localStorage.getItem('E_UserID'),
        value: '+1'
      }
    }];
    object[0].message.attribute.comment.qty = '+1';
    object[0].message.attribute.comment.user[0] = localStorage.getItem('E_UserID');
    backendObject = [{
      unique_id: parentuniqueid,
      sender_id: localStorage.getItem('E_UserID'),
      receiver_id: recieverid,
      message_type: 'ATTRIBUTE',
      attribute: 'comment',
      value: '+1',
      project_id: PROJECT_ID
    }];
    this.dataService.setMessageResObject(object);
    this.httpService.addAttribute(backendObject).subscribe(response => {
    });
  }

}
