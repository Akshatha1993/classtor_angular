import {Component, Input, OnInit} from '@angular/core';
import {Post} from '../feed/feed/feed.component';
import {Select} from '../post-question/post-question.component';
import {MatSnackBar} from '@angular/material';
import {HttpService} from '../../../core/http/http.service';
import {DataService} from '../../../core/services/data.service';
import * as sha1 from 'sha1/sha1';
import { PROJECT_ID } from '../../../../environments/environment';

@Component({
  selector: 'app-single-question-with-answers',
  templateUrl: './single-question-with-answers.component.html',
  styleUrls: ['./single-question-with-answers.component.css']
})
export class SingleQuestionWithAnswersComponent {
  @Input() postId: string;
  theQuestion: Post;

  public htmlContent: string;
  public showAnswerBar: boolean;


  askQuestionMapArray;
  questions;
  answers;
  displayableQuestion;
  contraPartyObject = {
    contraPartyId: '',
    contraPartyName: ''
  };
  comments = [];
  commentArray = [];
  initialDataFromLimit = 0;
  initialDataToLimit = 9;

  editorConfig = {
    contenteditable: true,
    translate: 'no',
    toolbar: [
      ['bold', 'italic', 'underline', 'strikeThrough', 'superscript', 'subscript'],
      ['fontName', 'fontSize', 'color'],
      ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'indent', 'outdent'],
      ['cut', 'copy', 'delete', 'removeFormat', 'undo', 'redo'],
      ['paragraph', 'blockquote', 'removeBlockquote', 'horizontalLine', 'orderedList', 'unorderedList'],
    ]
  };

  selectedAnonymityScopeId = 1;
  defaultAnonymityScope = '1';
  anonymityScope: Select[] = [
    {id: '0', userid: localStorage.getItem('E_UserID'), viewValue: 'Anonymous'},
    {id: '1', userid: localStorage.getItem('E_UserID'), viewValue: localStorage.getItem('E_UserName')},
  ];

  constructor(private snackBar: MatSnackBar,
              private dataService: DataService,
              private httpService: HttpService) {
    // this.theQuestion = {
    //   postId: 1,
    //   upvoted: false,
    //   author: 'Aakash Palimula',
    //   lastActive: '8 hours ago',
    //   profilePictureUrl: '',
    //   question: 'Friends, Today teacher told us that if i travel by train, even if i am sitting, that i am moving with respect to a stationary pole. Can someone please help me understand?',
    //   replyCount: 20,
    //   tag: 'Class 9 - Physics',
    //   upvoteCount: 14,
    //   flagged: false
    // };

    this.dataService.getMapArrayToMessage.subscribe(response => {
      this.comments = [];
      let convertedDateTime;
      let time;
      let convertedPoints;
      this.askQuestionMapArray = response;
      if (this.askQuestionMapArray !== '') {
        this.questions = this.askQuestionMapArray.get(localStorage.getItem('E_classId')).get('0');
        if (this.askQuestionMapArray.get(localStorage.getItem('E_classId')).get(localStorage.getItem('mesUniqueId')) !== undefined) {
          this.answers = this.askQuestionMapArray.get(localStorage.getItem('E_classId')).get(localStorage.getItem('mesUniqueId'));
          if (this.answers.length > 0) {
            for (let j = 0; j < this.answers.length; j++) {
              time = this.answers[j].message.time.slice(0, -3);
              convertedDateTime = this.time2TimeAgo(j, Number(time));
              this.answers[j].message['timezone'] = convertedDateTime;
              // if (this.askQuestionMapArray.get('ASK').get(this.answers[j].unique_id) !== undefined) {
              //   this.commentArray = this.askQuestionMapArray.get('ASK').get(this.answers[j].unique_id);
              //   this.comments.push(...this.commentArray);
              // }
            }
          }
        }
        for (let i = 0; i < this.questions.length; i++ ) {
          if (this.questions[i].unique_id === localStorage.getItem('mesUniqueId')) {
            this.displayableQuestion = this.questions[i];

            time = this.displayableQuestion.message.time.slice(0, -3);
            convertedDateTime = this.time2TimeAgo(i, Number(time));
            this.displayableQuestion.message['timezone'] = convertedDateTime;
          }
        }

        // for (let k = 0; k < this.comments.length; k++) {
        //   time = this.comments[k].message.time.slice(0, -3);
        //   convertedDateTime = this.time2TimeAgo(k, Number(time));
        //   this.comments[k].message['timezone'] = convertedDateTime;
        // }
      }
    });

    this.showAnswerBar = false;
  }

  time2TimeAgo(i, time) {
    // This function computes the delta between the
    // provided timestamp and the current time, then test
    // the delta for predefined ranges.

    const d = new Date();  // Gets the current time
    const nowTs = Math.floor(d.getTime() / 1000); // getTime() returns milliseconds, and we need seconds, hence the Math.floor and division by 1000
    const seconds = nowTs - time;

    // morethan one year.
    if (seconds > 3.154e+7) {
      return Math.floor(seconds / 3.154e+7) + ' years ago';
    }

    // more that two days
    if (seconds > 2 * 24 * 3600) {
      return Math.floor(seconds / 86400) + ' days ago';
    }
    // a day
    // if (seconds > 24 * 3600) {
    //   console.log(Math.floor(seconds / 86400) + 'yesterday');
    //   return Math.floor(seconds / 86400) + ' yesterday';
    // }

    if (seconds > 3600) {
      return Math.floor(seconds / 3600) + ' hour ago';
    }
    // if (seconds > 1800) {
    //   console.log('Half an hour ago');
    // }
    if (seconds > 60) {
      return Math.floor(seconds / 60) + ' minute ago';
    }

    if (seconds < 60) {
      return 'Just now';
    }
  }

  onSelectionChangedAnonymityLevel($event) {
    this.selectedAnonymityScopeId = $event.value;
    this.showSnackbarMessage('You will be posting as ' + this.getAnonymityScopeNameFromChosenAnonymityScopeId());
  }

  getAnonymityScopeNameFromChosenAnonymityScopeId() {
    for (const anonymityScope of this.anonymityScope) {
      if (anonymityScope.id === this.selectedAnonymityScopeId.toString()) {
        return anonymityScope.viewValue;
      }
    }
  }

  showSnackbarMessage(message: string) {
    this.snackBar.open(message, 'Close', {
      duration: 2000,
    });
  }

  postAnswer(question) {
    this.showSnackbarMessage(
      'PSEUDO POST QUESTION' + '\n' +
      'HTML Content: ' + this.htmlContent + '\n' +
      'Anonymity Level: ' + this.getAnonymityScopeNameFromChosenAnonymityScopeId() + '\n'
    );

    let messageStatus;
    let messageParent;
    messageParent = localStorage.getItem('mesUniqueId');
    let localCategory;
    localCategory = question.message.category;
    let responseObject;
    if (localStorage.getItem('E_UserClassType') === '1') {
      messageStatus = true;
    } else {
      messageStatus = false;
    }
    responseObject = [{
      unique_id: '',
      message: {
        parent: '',
        sender_points: '',
        unique_id: '',
        project_id: PROJECT_ID,
        receiver_id: question.message.receiver_id,
        receiver_name: question.message.receiver_name,
        sender_name: this.getAnonymityScopeNameFromChosenAnonymityScopeId(),
        sender_id: localStorage.getItem('E_UserID'),
        message_type: 'TEXT',
        message: {
          msg: '',
          teacher_message: messageStatus
        },
        receiver_type: 'I',
        time: '',
        category: localCategory,
        attribute: {
          like: {
            qty: '0',
            user: [],
          },
          dislike: {
            qty: '0',
            user: [],
          },
          comment: {
            qty: '0',
            user: [],
          },
          report: {
            qty: '0',
            user: [],
          }
        }

      },
      last_updated: ''
    }];
    let randomNum;
    let encryptedPassword;
    let stringTime;
    let hashedNum;
    let h;
    let unixTime;
    let timstamp;
    let object;
    let messagetext;
    messagetext = this.htmlContent;
    randomNum = Math.floor(Math.random() * 90000) + 10000;
    encryptedPassword = sha1(localStorage.getItem('E_UserID') + messagetext + randomNum);
    hashedNum = encryptedPassword.substring(0, 15);

    let result;
    let messageJson;
    responseObject[0].unique_id = (hashedNum);
    responseObject[0].message.message.msg = messagetext;
    responseObject[0].message.parent = messageParent;
    responseObject[0].message.unique_id = hashedNum;
    responseObject[0].message.sender_points = Number(localStorage.getItem('E_sender_points')) + 1;
    unixTime = new Date();
    timstamp = unixTime.getTime();
    h = timstamp;
    stringTime = h.toString();

    responseObject[0].message.time = stringTime;
    responseObject[0].last_updated = stringTime;
    messageJson = {
      msg: messagetext,
      teacher_message: messageStatus
    };

    object = [
      {
        unique_id: hashedNum,
        receiver_type: 'I',
        sender_name: this.getAnonymityScopeNameFromChosenAnonymityScopeId(),
        sender_id: localStorage.getItem('E_UserID'),
        receiver_name: question.message.receiver_name,
        receiver_id: question.message.receiver_id,
        sender_points: (Number(localStorage.getItem('E_sender_points')) + 1).toString(),
        message_type: 'TEXT',
        message: messageJson,
        parent: messageParent,
        project_id: PROJECT_ID,
        random_key: randomNum,
        category: localCategory,
        attribute: {
          like: {
            qty: '0',
            user: [],
          },
          dislike: {
            qty: '0',
            user: [],
          },
          comment: {
            qty: '0',
            user: [],
          },
          report: {
            qty: '0',
            user: [],
          }
        }
      }
    ];
    this.dataService.setMessageResObject(responseObject);
    localStorage.setItem('E_sender_points', (Number(localStorage.getItem('E_sender_points')) + 1).toString());
    this.sendCommentAttribute(messageParent, question.message.receiver_id);
    this.sendTophiePointsAttribute(hashedNum, question.message.receiver_id);
    this.htmlContent = '';
    this.showAnswerBar = !this.showAnswerBar;
    this.httpService.sendMessage(object).subscribe(response => {
      result = response;
      if (result.status === 'success') {
        this.htmlContent = '';
      }
    });
  }


  sendCommentAttribute(parentuniqueid, recieverid) {
    let object;
    let backendObject;
    object = [{
      last_updated: '',
      unique_id: '',
      read_status: 0,
      message: {
        unique_id: parentuniqueid,
        receiver_id: recieverid,
        message_type: 'ATTRIBUTE',
        attribute: {
          comment: {
            qty: '',
            user: [],
          }
        },
        attributeType: 'comment',
        sender_id: localStorage.getItem('E_UserID'),
        value: '+1'
      }
    }];
    object[0].message.attribute.comment.qty = '+1';
    object[0].message.attribute.comment.user[0] = localStorage.getItem('E_UserID');
    backendObject = [{
      unique_id: parentuniqueid,
      sender_id: localStorage.getItem('E_UserID'),
      receiver_id: recieverid,
      message_type: 'ATTRIBUTE',
      attribute: 'comment',
      value: '+1',
      project_id: PROJECT_ID
    }];
    this.dataService.setMessageResObject(object);
    this.httpService.addAttribute(backendObject).subscribe(response => {
    });
  }

  toggleShowAnswerBar() {
    this.showAnswerBar = !this.showAnswerBar;
  }

  sendTophiePointsAttribute(uniqueid, recieverid) {
    let backendObject;
    backendObject = [{
      unique_id: uniqueid,
      sender_id: localStorage.getItem('E_UserID'),
      receiver_id: recieverid,
      message_type: 'SCORE',
      attribute: 'POINTS',
      value: '+1',
      project_id: PROJECT_ID
    }];
    this.httpService.addAttribute(backendObject).subscribe(response => {
    });
  }
}
