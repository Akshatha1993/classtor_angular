import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatSnackBar} from '@angular/material';
// import {ApiManagerService} from '../../../../Shared/api-manager.service';
import {CreateClassModal} from '../../../../Modals/CreateClassModal';
import {Subscription} from 'rxjs';
import {HttpService} from '../../../../core/http/http.service';
import {DataService} from '../../../../core/services/data.service';

@Component({
  selector: 'app-create-classroom',
  templateUrl: './create-classroom.component.html',
  styleUrls: ['./create-classroom.component.css']
})
export class CreateClassroomComponent implements OnInit {

  apiCallSubscription: Subscription;
  @Output() proceedToAddStudentsToNewClassroom = new EventEmitter();
  @Output() ToPostsScreen = new EventEmitter();
  errorMessageStatus = false;

  constructor(private snackBar: MatSnackBar,
              private httpService: HttpService,
              private dataService: DataService) { }

  ngOnInit() {
  }

  createNewClass(classname: string, classId: string, classDescription: string) {
    if (classname === '' || classId === '' || classDescription === '') {
      this.errorMessageStatus = true;
    } else {
      this.httpService.createClassRoom(classname, classId, classDescription, '2').subscribe(response => {
        let apiResponse;
        apiResponse = response;
        // this.ToPostsScreen.emit();
        this.proceedToAddStudentsToNewClassroom.emit();
        this.dataService.setCreatedClass(classname);
        this.dataService.setrefreshCommunication(true);
      });
    }
  }


  onClassCreatedCallback(callBackData: string) {
    this.showSnackbarMessage(
      'Classroom successfully created. Add some students now.'
    );
    // Replace with API success protocol
    if (true) {
      this.proceedToAddStudentsToNewClassroom.emit();
    }
  }

  showSnackbarMessage(message: string) {
    this.snackBar.open(message, 'Close', {
      duration: 2000,
    });
  }
}
