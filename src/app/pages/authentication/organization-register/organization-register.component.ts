import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../core/services/data.service';
import {HttpService} from '../../../core/http/http.service';
declare var $: any;

@Component({
  selector: 'app-organization-register',
  templateUrl: './organization-register.component.html',
  styleUrls: ['./organization-register.component.css']
})
export class OrganizationRegisterComponent implements OnInit {

  orgRegisterForm: FormGroup;
  email;
  loginErrorStatus = false;
  showLogo = false;
  @Output() successfullySignedIn = new EventEmitter();
  @Output() skipToFeedsPage  = new EventEmitter();

  // For the Error Modal.
  errorModalMessage;
  errorModalHeading;
  errorModalBtnText;
  errorModalImgStatus;
  errorModalStatus = false;

  constructor(private dataService: DataService,
              private httpService: HttpService) {

  }

  ngOnInit() {
    this.orgRegisterForm = new FormGroup({
      orgname: new FormControl('', [Validators.required]),
      orgcode: new FormControl('', [Validators.required]),
    });

    const queryString = window.location.search.substring(1);
    if (queryString !== '') {
      const urlParams = queryString.split('&');
      const InstituteID = urlParams[0].split('=');
      if (InstituteID[1] !== 'e9Z5w8') {
        this.showLogo = true;
      }
      // this.email = ID[1];
    }
  }

  onSignIn() {
    if (this.orgRegisterForm.value.orgname === '' || this.orgRegisterForm.value.orgcode === '') {
      this.loginErrorStatus = true;
    } else {
        this.httpService.organisationDetail(this.orgRegisterForm.value.orgname,
          this.orgRegisterForm.value.orgcode, '2').subscribe(response => {
          let apiResponse = response;
          apiResponse = apiResponse['response']['result'];
          if (apiResponse['status'] !== 0) {

            if (apiResponse['status'] !== 0) {
              $.toast({
                heading: 'Success',
                text: 'Logged In successfully',
                bgColor: '#b3cde0',
                loaderBg: '#005b96',               // Background color for toast
                textColor: '#000',            // text color
                allowToastClose: true,       // Show the close button or not
                hideAfter: 4000,              // `false` to make it sticky or time in miliseconds to hide after
                position: 'top-right',
                icon: 'success'
              });
              localStorage.setItem('E_OrgId', apiResponse['output'].OrgID);
              localStorage.setItem('E_OrgName', apiResponse['output'].OrgName);
              this.successfullySignedIn.emit();
            }
          } else {
            const errorCode = apiResponse['code']['Code'];
            const errorDesc = [];
            const errorMessage = apiResponse['description'];
            for (const value in errorMessage) {
              errorDesc.push(errorMessage[value]);
            }
            this.errorModalHeading = 'Error';
            this.errorModalMessage = errorDesc[0];
            this.errorModalBtnText = '';
            this.errorModalImgStatus = true;
            this.errorModalStatus = true;
          }

        });
      }
  }

  closeErrorModal() {
    this.errorModalStatus = false;
  }

  skip() {
    this.skipToFeedsPage.emit();
  }

}
