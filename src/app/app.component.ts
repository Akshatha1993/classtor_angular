import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Stomp} from 'stompjs/lib/stomp';
import * as SockJS from 'sockjs-client';
import {HttpService} from './core/http/http.service';
import {DataService} from './core/services/data.service';
import {PROJECT_CONNECT_SOCKET_PATH} from '../environments/environment';
import * as sha1 from 'sha1/sha1';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  stompClient;
  kafkaMsgArray;
  finalMessagingMap = new Map();
  masterMessagingMap = new Map();
  parentidMap = new Map();
  levelZeroAscending = false;
  nonZeroAscending = false;
  finalArray;
  socketConnected = false;
  lastUpdatedTimeOfParent;
  userName;
  firstTimeDataFlag = false;
  socketConnectionIds = [];
  socketIds = [];
  parentid;
  result;
  likeCountDigit = 0;
  dislikeCountDigit = 0;
  commentCountDigit = 0;
  reportCountDigit = 0;
// while refresh same message will come again , in that case we should not increase the comment count
  skipStatus = false;


  constructor(private httpService: HttpService,
              private http: HttpClient,
              private dataService: DataService,
              private spinner: NgxSpinnerService) {


    this.kafkaMsgArray = [];

    // this.dataService.getrefreshCommunication.subscribe( refreshRes => {
    //   let response;
    //   response = refreshRes;
    //   if (response !== '') {
    //     if (response === true) {
    //       if (localStorage.getItem('E_UserID') === null) {
    //       } else {
    //         this.connectScoket(localStorage.getItem('E_UserID'));
    //       }
    //     }
    //   }
    // });

    this.dataService.getReadStatus.subscribe(response => {

      this.parentid = response;

      if (this.parentid !== '') {
        this.updateParentReadStatus(this.parentid);
      }
    });

    let result;
    this.dataService.getresponseObject.subscribe(response => {
      // this will be called when the user sends message
      if (response !== '') {
        this.result = response;
        this.kafkaMsgArray = this.result;
        this.messageResponseObject(this.result, false);
      }
    });

    this.dataService.getInitiatedCommunication.subscribe(initComnresult => {
      this.socketIds = [];
      let initiatedCommunication;
      initiatedCommunication = initComnresult;
      if (initiatedCommunication === '0') {
        localStorage.setItem('E_InitiateCommunication', '1');
        // this.socketConnectionIds = JSON.parse(localStorage.getItem('E_ClassList'));
        // for (let i = 0; i < this.socketConnectionIds.length; i++) {
        //   this.socketIds.push(this.socketConnectionIds[i].group_key);
        // }
        this.socketIds = JSON.parse(localStorage.getItem('E_socketIdConnection'));
        // localStorage.setItem('E_socketIdConnection', JSON.stringify(this.socketIds));
        this.connectScoket(this.socketIds, localStorage.getItem('E_UserID'));
      }
    });

  }

  messageResponseObject(message, msgFromSocket) {
    let result;
    this.kafkaMsgArray = message;

    if (msgFromSocket === true && this.kafkaMsgArray[0].message.message_type === 'ATTRIBUTE') {
      return;
    }

    if (this.kafkaMsgArray[0].message.message_type !== 'ATTRIBUTE' && this.kafkaMsgArray[0].message.message_type !== 'ACK') {
      for (let i = 0; i < this.kafkaMsgArray.length; i++) {
        this.getFinalMEssagingMap(this.kafkaMsgArray[i], this.kafkaMsgArray[i].message.category);
        // this will be called when the kafka socket enables
      }
    }

    this.getDisplayableMap(msgFromSocket);
    // to update the parent's parent time
    if (this.kafkaMsgArray.length === 1 && this.kafkaMsgArray[0].message.message_type !== 'ACK' && this.kafkaMsgArray[0].message.message_type !== 'ATTRIBUTE') {
      this.updateParentTime(this.kafkaMsgArray[0], this.kafkaMsgArray[0].message.parent, this.kafkaMsgArray[0].last_updated, this.skipStatus, msgFromSocket);
    }

    // // set Master mesaage map
    if (this.kafkaMsgArray[0].message.message_type !== 'ATTRIBUTE' && this.kafkaMsgArray[0].message.message_type !== 'ACK') {
      for (let i = 0; i < this.kafkaMsgArray.length; i++) {
        this.setMasterMessagingMap(this.kafkaMsgArray[i], this.kafkaMsgArray[i].message.category);
      }
    }
    this.firstTimeDataFlag = true;
    this.dataService.setMessages(this.masterMessagingMap);
  }

  ngOnInit() {
    // this.connectScoket(localStorage.getItem('E_UserID'));
  }

  getDisplayableMap(msgFromSocket) {
    let existingMapMsgArray;
    let timestamp;
    let likeObject;
    for (let j = 0; j < this.kafkaMsgArray.length; j++) {
      // if the socket response gives message type if ACK update the read status of the message then break
      if (this.kafkaMsgArray[j].message.message_type === 'ACK') {
        this.updateReadStatus(this.kafkaMsgArray[j]);
        break;
      } else if (msgFromSocket === false && this.kafkaMsgArray[j].message.message_type === 'ATTRIBUTE') {
        this.likeCount(this.kafkaMsgArray[j]);
        break;
      }

      existingMapMsgArray = [];
      // timestamp = JSON.parse(this.kafkaMsgArray[j].last_updated);

      // this is required to prevent messages sent from UI getting duplicated,
      // one from added entey directly and another from socket.
      if (this.parentidMap.has(this.kafkaMsgArray[j].unique_id)) {
        //
        this.skipStatus = true;
        continue;
      }
      this.skipStatus = false;
      // this is a array to maintain the data for a particular instance.
      // Check whether the Parent ID is already present in the MAP
      // this has() method will check whether the passed key is already present or not on in the map. (it gives either true / false )
      if (this.finalMessagingMap.has(this.kafkaMsgArray[j].message.parent)) {
        // parentTimeUpdate[j].last_updated = this.lastUpdatedTimeOfParent;
        // Do this for loop only if (Parent =0  and levelZeroAscending = True   OR    Parent !=0  and NonZeroAscending = True)
        if (msgFromSocket === true && this.kafkaMsgArray[j].message.attribute !== '{}') {
          this.kafkaMsgArray[j].message.attribute = JSON.parse(this.kafkaMsgArray[j].message.attribute);
        }

        if ((this.kafkaMsgArray[j].message.parent === '0' && this.levelZeroAscending === true) || (this.kafkaMsgArray[j].message.parent !== '0' && this.nonZeroAscending === true)) {
          this.setExistingMapArrayAscending(j);
        } else {
          this.setExistingMapArrayDescending(j);
        }
      } else {
        if (msgFromSocket === true && this.kafkaMsgArray[j].message.attribute !== '{}') {
          this.kafkaMsgArray[j].message.attribute = JSON.parse(this.kafkaMsgArray[j].message.attribute);
        }
        // if the parent id is not present in the MAP then the object is pushed into an array and set to the ID in the MAP.
        existingMapMsgArray.push(this.kafkaMsgArray[j]);
        // And finally the array data is set into the MAP to the respective ID.
        this.finalMessagingMap.set(this.kafkaMsgArray[j].message.parent, existingMapMsgArray);
        if (this.parentidMap.has(this.kafkaMsgArray[j].message.parent)) {
          //;
        } else {
          // this.httpService.getGroupMessages(localStorage.getItem('E_UserID'), this.kafkaMsgArray[j].message.parent, 0, 20, 'PARENTID',  '', localStorage.getItem('E_UserID'),  '').subscribe(response => {
          //   let result;
          //   result = response;
          // });
        }
      }
      this.parentidMap.set(this.kafkaMsgArray[j].unique_id, this.kafkaMsgArray[j].message.parent);
    }
    if (this.kafkaMsgArray[0].message.parent !== undefined) {
      this.refreshReadToken(this.kafkaMsgArray[0].message.parent, localStorage.getItem('E_UserID'));
    }
  }


  incrementParentAttributeCommentCount(message) {
    if (this.parentidMap.has(message.unique_id)) {

    } else {
      let parentid;
      parentid = this.parentidMap.get(message.message.parent);
      let parentMessage;
      let finalMessagesList;
      finalMessagesList = this.finalMessagingMap.get(parentid);
      for (let i = 0; i < finalMessagesList.length; i++) {
        if (finalMessagesList[i].unique_id === message.message.parent) {
          parentMessage = finalMessagesList[i];
        }
        let count;
        if (parentMessage.message.attribute.comment.user.length > 0) {
          for (let j = 0; j < parentMessage.message.attribute.comment.user.length; j++) {
            if (parentMessage.message.attribute.comment.user[j].indexOf(localStorage.getItem('E_UserID')) > 0 || parentMessage.message.attribute.comment.user[j].indexOf(localStorage.getItem('E_UserID')) === 0) {
            } else {
              count = parentMessage.message.attribute.comment.qty;
              parentMessage.message.attribute.comment.user.push(localStorage.getItem('E_UserID'));
              parentMessage.message.attribute.comment.qty = Number(count) + 1;
            }
          }
        } else {
          parentMessage.message.attribute.comment.user.push(localStorage.getItem('E_UserID'));
          parentMessage.message.attribute.comment.qty = '1';
        }
        // finalMessagesList.splice(1, parentMessage);
        // this.finalMessagingMap.set(parentMessage.message.parent, finalMessagesList);
        // this.setMasterMessagingMap(parentMessage, parentMessage.message.category);
        // break;
      }
    }
  }

  likeCount(message) {
    let parentid;
    let childArray = [];
    let users = [];
    let displikeUser = [];
    parentid = this.parentidMap.get(message.message.unique_id);
    childArray = this.finalMessagingMap.get(parentid);
    for (let i = 0; i < childArray.length; i++) {
      if (message.message.unique_id === childArray[i].unique_id) {
        if (message.message.attributeType === 'like') {
          this.likeCountDigit = childArray[i].message.attribute.like.qty;
          this.dislikeCountDigit = childArray[i].message.attribute.dislike.qty;
          // users[i] = (childArray[i].message.attribute.like.user);
          displikeUser[i] = (childArray[i].message.attribute.dislike.user);
          if (displikeUser[i].indexOf(localStorage.getItem('E_UserID')) > 0 || displikeUser[i].indexOf(localStorage.getItem('E_UserID')) === 0) {
            const index = displikeUser[i].indexOf(message.message.attribute.like.user[0]);
            displikeUser[i].splice(index, 1);
            this.dislikeCountDigit = Number(this.dislikeCountDigit) - 1;
            users.push(message.message.attribute.like.user[0]);
            this.likeCountDigit = Number(this.likeCountDigit) + 1;
          } else {
            if (message.message.attribute.like.qty === '+1') {
              users.push(message.message.attribute.like.user[0]);
              this.likeCountDigit = Number(this.likeCountDigit) + 1;
              // childArray[i].message.attribute.like['childLikeButton'] = true;
            }
          }
          // }
          childArray[i].message.attribute.like.user = users;
          childArray[i].message.attribute.like.qty = this.likeCountDigit;
          childArray[i].message.attribute.dislike.user = displikeUser;
          childArray[i].message.attribute.dislike.qty = this.dislikeCountDigit;
        } else if (message.message.attributeType === 'dislike') {
          this.dislikeCountDigit = childArray[i].message.attribute.dislike.qty;
          this.likeCountDigit = childArray[i].message.attribute.like.qty;
          users[i] = (childArray[i].message.attribute.like.user);
          if (users[i].indexOf(localStorage.getItem('E_UserID')) > 0 || users[i].indexOf(localStorage.getItem('E_UserID')) === 0) {
            this.likeCountDigit = this.likeCountDigit - 1;
            this.dislikeCountDigit = Number(this.dislikeCountDigit) + 1;
            const index = users[i].indexOf(childArray[i].message.attribute.like.user[0]);
            users[i].splice(index, 1);
            displikeUser.push(message.message.attribute.dislike.user[0]);
          } else {
            displikeUser.push(message.message.attribute.dislike.user[0]);
            this.dislikeCountDigit = Number(this.dislikeCountDigit) + 1;
          }
          childArray[i].message.attribute.dislike.user = displikeUser;
          childArray[i].message.attribute.like.user = users;
          childArray[i].message.attribute.dislike.qty = this.dislikeCountDigit;
          childArray[i].message.attribute.like.qty = this.likeCountDigit;
        } else if (message.message.attributeType === 'report') {
          // message.message.attribute = JSON.parse(message.message.attribute);
          let reportUser = [];
          reportUser[i] = (childArray[i].message.attribute.report.user);
          this.reportCountDigit = childArray[i].message.attribute.report.qty;
          if (reportUser[i].indexOf(localStorage.getItem('E_UserID')) > 0 || reportUser[i].indexOf(localStorage.getItem('E_UserID')) === 0) {
            this.reportCountDigit = Number(this.reportCountDigit);
            // childArray[i].message.attribute.comment.qty = this.commentCountDigit;
            // childArray[i].message.attribute.comment.user = commentUser;
          } else {
            reportUser.push(message.message.attribute.report.user[0]);
            this.reportCountDigit = Number(this.reportCountDigit) + 1;
          }
          childArray[i].message.attribute.report.user = reportUser;
          childArray[i].message.attribute.report.qty = this.reportCountDigit;
        } else {
          // message.message.attribute = JSON.parse(message.message.attribute);
          let commentUser = [];
          // commentUser[i] = (childArray[i].message.attribute.comment.user);
          this.commentCountDigit = childArray[i].message.attribute.comment.qty;
          if (childArray[i].message.attribute.comment.user.indexOf(localStorage.getItem('E_UserID')) > 0 || childArray[i].message.attribute.comment.user.indexOf(localStorage.getItem('E_UserID')) === 0) {
            // this.commentCountDigit = Number(this.commentCountDigit);
            // childArray[i].message.attribute.comment.qty = this.commentCountDigit;
            // childArray[i].message.attribute.comment.user = commentUser;
          } else {
            childArray[i].message.attribute.comment.user.push(message.message.attribute.comment.user[0]);
            childArray[i].message.attribute.comment.qty = Number(childArray[i].message.attribute.comment.qty) + 1;
          }
          // childArray[i].message.attribute.comment.user = commentUser;
          // childArray[i].message.attribute.comment.qty = this.commentCountDigit;
        }

        childArray.splice(i, 1, childArray[i]);
        this.finalMessagingMap.set(childArray[i].message.parent, childArray);
        this.setMasterMessagingMap(childArray[i], childArray[i].message.category);
        break;
      }
    }
  }

  updateParentReadStatus(parentid) {
    let arrayOfChild: any[];
    let parentParentId;
    let parentElement;
    arrayOfChild = this.finalMessagingMap.get(parentid);
    for (let i = 0; i < arrayOfChild.length; i++) {
      if (arrayOfChild[i].message.sender_id !== localStorage.getItem('E_UserID')) {
        arrayOfChild[i].read_status = 1;
      }
    }
    this.finalMessagingMap.set(parentid, arrayOfChild);
    parentParentId = this.parentidMap.get(parentid);
    parentElement = this.finalMessagingMap.get(parentParentId);
    for (let j = 0; j < parentElement.length; j++) {
      if (parentElement[j].unique_id === parentid) {
        parentElement[j].read_status = 1;
      }
    }
    this.finalMessagingMap.set(parentParentId, parentElement);
    this.refreshReadToken(parentParentId, localStorage.getItem('E_UserID'));
  }

  refreshReadToken(parentid, E_UserID) {

    if (parentid === '0') {
      return;
    }
    let loopflag: boolean;
    loopflag = true;
    let arrayOfChild = [];
    let parentParentId;
    let parentElement = [];
    let readFlag;
    readFlag = 1;
    while (loopflag) {
      arrayOfChild = this.finalMessagingMap.get(parentid);
      for (let i = 0; i < arrayOfChild.length; i++) {
        if (arrayOfChild[i].message.sender_id !== E_UserID) {
          readFlag = readFlag && arrayOfChild[i].read_status;
          if (!readFlag) {
            break;
          }
        }
      }
      parentParentId = this.parentidMap.get(parentid);
      parentElement = this.finalMessagingMap.get(parentParentId);
      for (let j = 0; j < parentElement.length; j++) {
        if (parentElement[j].unique_id === parentid) {
          parentElement[j].read_status = readFlag;
        }
      }
      this.finalMessagingMap.set(parentParentId, parentElement);
      parentid = parentParentId;
      if (parentid === '0') {
        loopflag = false;
      }
    }
  }

  // update the read status manually without depending on socket response
  updateReadStatus(ackMessage) {
    let ackmessageParentId;
    let arrayOfChild;
    arrayOfChild = [];
    // first get the parent id of the acknowledge message which we get from the socket response from where we have formed the parentid and unique id map
    ackmessageParentId = this.parentidMap.get(ackMessage.message.unique_id[0]);
    // after getting parentid, by using this parent id get the array of child from the finalMessagingMap
    arrayOfChild = this.finalMessagingMap.get(ackmessageParentId);
    // loop through the unique id which we are updating the read status which is in array form
    for (let k = 0; k < ackMessage.message.unique_id.length; k++) {
      // loop through the arrayOfChild
      for (let c = 0; c < arrayOfChild.length; c++) {
        // check the unique id of each arrayOfChild which is matching to unique id of read status acknowledge message unique id
        if (arrayOfChild[c].unique_id === ackMessage.message.unique_id[k]) {
          // if equal then update the each arrayOfChild read status to 1
          arrayOfChild[c].read_status = ackMessage.read_status;
          // then break the for loop
          break;
        }
      }
    }
    // update the finalMessagingMap by passing parent id and arrayOfChild
    this.finalMessagingMap.set(ackmessageParentId, arrayOfChild);
  }

// update the parent time of each element
  updateParentTime(message, parent, lastupdatedtime, skipStatus, msgFromSocket) {
    let originalParent;
    originalParent = parent;
    // if the parent is '0' don't do anything return the object and come out
    if (parent === '0') {
      return;
    }
    // else initialize the arrayOfChilds as empty array
    let loopFlag: boolean;
    let parentParentId;
    let arrayOfChild = [];
    let parentMessage;
    // initialize loopflag as true;
    loopFlag = true;

    // while loop helps to loop each and every child messages in a map
    while (loopFlag) {
      // get the value of parent which we get from the parentidMap and initialize to parentParentId
      parentParentId = this.parentidMap.get(parent);
      // from finalMessagingMap main map get the values of parentParentId
      arrayOfChild = this.finalMessagingMap.get(parentParentId);
      // this condition is to check the array is ascending or descending
      if ((parentParentId === '0' && this.levelZeroAscending === true) || (parentParentId !== '0' && this.nonZeroAscending === true)) {
        // loop through the arrayOfChild
        for (let z = arrayOfChild.length - 1; z >= 0; z--) {
          // check each element's unique_id and the parent value
          if (arrayOfChild[z].unique_id === parent) {
            // if both are equal push arrayOfChild to parentMessage object to update the time
            parentMessage = arrayOfChild[z];
            // if the arrayOfChild length is only one just update the time of the perticular parent and set the value to map and break it
            if (arrayOfChild.length === 1) {
              arrayOfChild[z].last_updated = lastupdatedtime;
              if (originalParent === parent && skipStatus === false && msgFromSocket === true) {
                if (arrayOfChild[z].message.attribute.comment.user.length > 0) {
                  if (arrayOfChild[z].message.attribute.comment.user.indexOf(message.message.sender_id) > 0 || parentMessage.message.attribute.comment.user.indexOf(message.message.sender_id) === 0) {
                  } else {
                    arrayOfChild[z].message.attribute.comment.user.push(message.message.sender_id);
                    arrayOfChild[z].message.attribute.comment.qty = Number(arrayOfChild[z].message.attribute.comment.qty) + 1;
                  }
                } else {
                  arrayOfChild[z].message.attribute.comment.user.push(message.message.sender_id);
                  arrayOfChild[z].message.attribute.comment.qty = 1;
                }
                this.finalMessagingMap.set(parentParentId, arrayOfChild);
                this.setMasterMessagingMap(arrayOfChild[z], arrayOfChild[z].message.category);
                break;
              }
            }
            // else update the time and splice from that position the particular object push it at the end of arrayOfChild
            parentMessage.last_updated = lastupdatedtime;
            arrayOfChild.splice(z, 1);
            arrayOfChild.push(parentMessage);
            if (originalParent === parent && skipStatus === false && msgFromSocket === true) {
              if (arrayOfChild[z].message.attribute.comment.user.length > 0) {
                if (arrayOfChild[z].message.attribute.comment.user.indexOf(message.message.sender_id) > 0 || parentMessage.message.attribute.comment.user.indexOf(message.message.sender_id) === 0) {
                } else {
                  arrayOfChild[z].message.attribute.comment.user.push(message.message.sender_id);
                  arrayOfChild[z].message.attribute.comment.qty = Number(arrayOfChild[z].message.attribute.comment.qty) + 1;
                }
              } else {
                arrayOfChild[z].message.attribute.comment.user.push(message.message.sender_id);
                arrayOfChild[z].message.attribute.comment.qty = 1;
              }
              this.finalMessagingMap.set(parentParentId, arrayOfChild);
              this.setMasterMessagingMap(arrayOfChild[z], arrayOfChild[z].message.category);
              break;
            }
            // this.setMasterMessagingMap(arrayOfChild[z], arrayOfChild[z].message.category);
          }
        }
        // again set the value of parentParentId to parent and loop through each parent
        parent = parentParentId;
        // at the end when the parent becomes zero make the loopFlag false and stop
        if (parent === '0') {
          loopFlag = false;
        }
      } else {
        // descending loop. Loop the array from the last position
        for (let z = 0; z <= arrayOfChild.length - 1; z++) {
          // check each element's unique_id and the parent value
          if (arrayOfChild[z].unique_id === parent) {
            // if both are equal push arrayOfChild to parentMessage object to update the time
            parentMessage = arrayOfChild[z];
            // if the arrayOfChild length is only one just update the time of the perticular parent and set the value to map and break it
            if (arrayOfChild.length === 1) {
              arrayOfChild[z].last_updated = lastupdatedtime;
              if (originalParent === parent && skipStatus === false && msgFromSocket === true) {
                if (arrayOfChild[z].message.attribute.comment.user.length > 0) {
                  if (arrayOfChild[z].message.attribute.comment.user.indexOf(message.message.sender_id) > 0 || parentMessage.message.attribute.comment.user.indexOf(message.message.sender_id) === 0) {
                  } else {
                    arrayOfChild[z].message.attribute.comment.user.push(message.message.sender_id);
                    arrayOfChild[z].message.attribute.comment.qty = Number(arrayOfChild[z].message.attribute.comment.qty) + 1;
                  }
                } else {
                  arrayOfChild[z].message.attribute.comment.user.push(message.message.sender_id);
                  arrayOfChild[z].message.attribute.comment.qty = 1;
                }
                this.finalMessagingMap.set(parentParentId, arrayOfChild);
                this.setMasterMessagingMap(arrayOfChild[z], arrayOfChild[z].message.category);
                break;
              }
            }
            // else update the time and splice from that position the particular object push it at the end of arrayOfChild
            parentMessage.last_updated = lastupdatedtime;
            arrayOfChild.splice(z, 1);
            // for i  from 0 to len -1
            for (let i = 0; i < arrayOfChild.length; i++) {
              // check arrayOfChild last updated time is greater than the lastupdatedtime
              if (arrayOfChild[i].last_updated > lastupdatedtime) {
                // check lenght of the arrayOfChild is last
                if (i === arrayOfChild.length - 1) {
                  // then push and break
                  arrayOfChild.push(parentMessage);
                  break;
                } else {
                  continue;
                }
                // check arrayOfChild last updated time is less than or equal the lastupdatedtime splice and insert into particular position
              } else if (arrayOfChild[i].last_updated <= lastupdatedtime) {
                arrayOfChild.splice(i, 0, parentMessage);
                break;
              }
            }
            // arrayOfChild.unshift(parentMessage);
            if (originalParent === parent && skipStatus === false && msgFromSocket === true) {
              if (arrayOfChild[z].message.attribute.comment.user.length > 0) {
                if (arrayOfChild[z].message.attribute.comment.user.indexOf(message.message.sender_id) > 0 || parentMessage.message.attribute.comment.user.indexOf(message.message.sender_id) === 0) {
                } else {
                  arrayOfChild[z].message.attribute.comment.user.push(message.message.sender_id);
                  arrayOfChild[z].message.attribute.comment.qty = Number(arrayOfChild[z].message.attribute.comment.qty) + 1;
                }
              } else {
                arrayOfChild[z].message.attribute.comment.user.push(message.message.sender_id);
                arrayOfChild[z].message.attribute.comment.qty = 1;
              }
              this.finalMessagingMap.set(parentParentId, arrayOfChild);
              this.setMasterMessagingMap(arrayOfChild, arrayOfChild[z].message.category);
              break;
            }
          }
        }
      parent = parentParentId;
      if (parent === '0') {
        loopFlag = false;
      }
    }
  }
  }

  setExistingMapArrayAscending(j) {
    let existingMapArray = [];
    let arrayLength;
    let elementTime;
    // if we have the key id already in the map then get the values of the key and assign it to the array.
    existingMapArray = this.finalMessagingMap.get(this.kafkaMsgArray[j].message.parent);
    // now the new object can be pushed into the existing array so that older data is not lost.

    arrayLength = existingMapArray.length;
    // we are checking from the last element of the finalMessagingMap and looping from backward
    for (let k = arrayLength - 1; k >= 0; k--) {
      elementTime = existingMapArray[k].last_updated;
      // checking the incoming message's time whether it is less than the existing last element object time
      // new messages are expected to match this condition at first instance rather iterating through.
      if (elementTime < this.kafkaMsgArray[j].last_updated) {
        // if last element  just push (ideally every new message)
        if (k === arrayLength - 1) {
          existingMapArray.push(this.kafkaMsgArray[j]);
          break;
        } else {
          // or else splice and insert
          existingMapArray.splice(k + 1, 0, this.kafkaMsgArray[j]);
          break;
        }
      } else if (elementTime === this.kafkaMsgArray[j].last_updated) {
        // checking whether the time is equal then we are checking the unique id of each
        if (existingMapArray[k].unique_id === this.kafkaMsgArray[j].unique_id) {
          // if same unique id do nothing because it is ducplicate
          break;
        } else {
          // continue process
          if (k === 0) {
            existingMapArray.splice(k, 0, this.kafkaMsgArray[j]);
          } else {
            continue;
          }
        }
      } else if (elementTime > this.kafkaMsgArray[j].last_updated) {
        // checking last element time is greater than the incoming message
        if (k === 0) {
          // if so push it at the 0th position
          existingMapArray.unshift(this.kafkaMsgArray[j]);
        } else {
          continue;
        }
      }
    }
    // And finally the array data is set into the MAP to the respective ID.
    this.finalMessagingMap.set(this.kafkaMsgArray[j].message.parent, existingMapArray);
  }

  setExistingMapArrayDescending(j) {
    let existingMapArray = [];
    let arrayLength;
    let elementTime;
    // if we have the key id already in the map then get the values of the key and assign it to the array.
    existingMapArray = this.finalMessagingMap.get(this.kafkaMsgArray[j].message.parent);
    // now the new object can be pushed into the existing array so that older data is not lost.

    arrayLength = existingMapArray.length;
    // we are checking from the last element of the finalMessagingMap and looping from backward
    for (let k = 0; k < arrayLength; k++) {
      elementTime = existingMapArray[k].last_updated;
      // checking the incoming message's time whether it is less than the existing last element object time
      // new messages are expected to match this condition at first instance rather iterating through.
      if (elementTime < this.kafkaMsgArray[j].last_updated) {
        // if last element  just push (ideally every new message)
        if (k === 0) {
          existingMapArray.unshift(this.kafkaMsgArray[j]);
          break;
        } else {
          // or else splice and insert
          existingMapArray.splice(k, 0, this.kafkaMsgArray[j]);
          break;
        }
      } else if (elementTime === this.kafkaMsgArray[j].last_updated) {
        // checking whether the time is equal then we are checking the unique id of each
        if (existingMapArray[k].unique_id === this.kafkaMsgArray[j].unique_id) {
          // if same unique id do nothing because it is ducplicate
          break;
        } else {
          // continue process
          if (k === arrayLength - 1) {
            existingMapArray.push(this.kafkaMsgArray[j]);
          } else {
            continue;
          }
        }
      } else if (elementTime > this.kafkaMsgArray[j].last_updated) {
        // checking last element time is greater than the incoming message
        if (k === arrayLength - 1) {
          // if so push it at the 0th position
          existingMapArray.push(this.kafkaMsgArray[j]);
        } else {
          continue;
        }
      }
    }
    // And finally the array data is set into the MAP to the respective ID.
    this.finalMessagingMap.set(this.kafkaMsgArray[j].message.parent, existingMapArray);
  }

  getFinalMEssagingMap(message, key: string) {
    if (this.masterMessagingMap.has(key)) {
      this.finalMessagingMap = this.masterMessagingMap.get(key);
    } else {
      this.finalMessagingMap = new Map();
    }
  }

  setMasterMessagingMap(message, key: string) {
    this.masterMessagingMap.set(key, this.finalMessagingMap);
  }

  connectScoket(socketconnid, userid) {

    if (this.socketConnected === true) {
      this.stompClient.disconnect();
      // this.spinner.show();
      this.socketConnected = false;
    }

    let socket;
    let socketid;
    let message2;
    // socketid = [userid, 'abc', 'def', 'ghk'];
    socket = new SockJS(PROJECT_CONNECT_SOCKET_PATH);
    // const socket = Socket(this.serverUrl);
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, () => {
      this.masterMessagingMap.clear();
      this.finalMessagingMap.clear();
      this.parentidMap.clear();
      this.spinner.show();
      for (let i = 0; i < socketconnid.length; i++) {
        this.stompClient.subscribe('/topic/' + socketconnid[i], (message) => {
          // const payload = JSON.parse(message.body);
          message2 = JSON.parse(message.body);
          for (let i = 0; i < message2.length; i++) {
            // this.dataService.getresponseObject.subscribe( response => {
            //   if (response === '') {
            if (message2[i].message.message_type !== 'ACK' && message2[i].message.message_type !== 'ATTRIBUTE') {
              message2[i].message.message = JSON.parse(message2[i].message.message);
            }
            //   }
            // });
          }
          this.messageResponseObject(message2, true);
        });
      }
      if (this.socketConnected === false) {

        this.httpService.getGroupMessages(userid, '0', 0, 20, 'PARENTID', '', socketconnid.toString(), '').subscribe(response => {
          let result;
          result = response;
          if (result.status === 'success') {
            this.socketConnected = true;
          }
        });
        this.httpService.fetchSenderPoints().subscribe(apiresponse => {
          localStorage.setItem('E_sender_points', apiresponse['points']);
        });
      }
    }, this.onError());

  }

  onError() {
    console.log('Could not connect to WebSocket server. Please refresh this page to try again');
  }
}
